#ifdef atmr_msgs2_1_1_as_txt_cpn

#define p_cnt       c_expr { now.p14_g_cnt >= 0 && now.p14_g_cnt <= 1 }
#define p_full_c1   c_expr { now.p6_station_c.content[0].field_2 == 2 }
#define p_empty_c1  c_expr { now.p6_station_c.content[0].field_2 == 0 }
#define p_reset_st1 c_expr { now.p16_station_reset.content[0] == 1 }
#define p_send_st1  c_expr { now.p2_station_send.content[0] == 1 }
#define p_req_st1   c_expr { now.p4_station_req.content[0].field_2 != 0 }
#define p_ack_st1   c_expr { now.p9_station_ack.content[0].field_2 != 0 }

#else

#define p_cnt       c_expr { now.p16_g_cnt >= 0 && now.p16_g_cnt <= 1 }
#define p_full_c1   c_expr { now.p7_station_c.content[0].field_2 == 2 }
#define p_empty_c1  c_expr { now.p7_station_c.content[0].field_2 == 0 }
#define p_reset_st1 c_expr { now.p1_station_reset.content[0] == 1 }
#define p_send_st1  c_expr { now.p3_station_send.content[0] == 1 }
#define p_req_st1   c_expr { now.p5_station_req.content[0].field_2 != 0 }
#define p_ack_st1   c_expr { now.p10_station_ack.content[0].field_2 != 0 }

#endif
