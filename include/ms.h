// <ms-datatype-methods> (MS_MAX_SIZE and methods cmp_##TYPENAME & null_##TYPENAME should be defined)
#define GEN_MS_METHODS(TYPENAME) \
 \
typedef struct msOf##TYPENAME { \
	inttype size; \
	cs_##TYPENAME content[MS_MAX_SIZE]; \
} msOf##TYPENAME; \
 \
void empty_msOf##TYPENAME(msOf##TYPENAME *ms) { \
	int i; \
	for (i = 0; i < MS_MAX_SIZE; ++i) { \
		null_##TYPENAME(&ms->content[i]); \
	} \
	ms->size = 0; \
} \
 \
void copy_msOf##TYPENAME(msOf##TYPENAME *ms, cs_##TYPENAME *res, int index) { \
	*res = ms->content[index]; \
} \
 \
void get_msOf##TYPENAME(msOf##TYPENAME *ms, cs_##TYPENAME *res, int index) { \
	*res = ms->content[index]; \
	for (; index < ms->size; ++index) { \
		ms->content[index] = ms->content[index + 1]; \
	} \
	null_##TYPENAME(&ms->content[--ms->size]); \
} \
 \
void put_msOf##TYPENAME(msOf##TYPENAME *ms, cs_##TYPENAME *elem) { \
	int i; \
	for (i = ms->size++; i > 0; --i) { \
		if (cmp_##TYPENAME(&ms->content[i - 1], elem) <= 0) break; \
		ms->content[i] = ms->content[i - 1]; \
	} \
	ms->content[i] = *elem; \
}
// </ ms-datatype-methods>

// <ms-1-datatype-methods>
#define GEN_MS1_METHODS(TYPENAME, NUMBER) \
 \
typedef struct ms1Of##TYPENAME { \
	inttype count[NUMBER]; \
} ms1Of##TYPENAME; \
 \
void empty_ms1Of##TYPENAME(ms1Of##TYPENAME *ms1) { \
	int i; \
	for (i = 0; i < NUMBER; ++i) { \
		ms1->count[i] = 0; \
	} \
} \
 \
void get_ms1Of##TYPENAME(ms1Of##TYPENAME *ms1, cs_##TYPENAME elem) { \
	--ms1->count[elem]; \
} \
 \
void put_ms1Of##TYPENAME(ms1Of##TYPENAME *ms1, cs_##TYPENAME elem) { \
	++ms1->count[elem]; \
}
// </ ms-1-datatype-methods>
