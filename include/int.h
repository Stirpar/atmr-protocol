void abs(int *res, int *val) {
	*res = *val < 0 ? -*val : *val;
}

int fails_abs(int *expr_error, int *evaluation_fail, int* val) {
	return false;
}

void min(int *res, int *val1, int *val2) {
	*res = *val1 < val2 ? *val1 : *val2;
}

int fails_min(int *expr_error, int *evaluation_fail, int *val1, int *val2) {
	return false;
}

void max(int *res, int *val1, int *val2) {
	*res = *val1 > val2 ? *val1 : *val2;
}

int fails_max(int *expr_error, int *evaluation_fail, int *val1, int *val2) {
	return false;
}
