/*
*  &&&   &&&   &   &    |\   @@@          @
* &   &  &  &  &&  & ---+ \  @  @   @@@   @
* &      &  &  & & &       x @  @  @ @ @  @
* &   &  &&&   &  && ---+ /  @@@   @ @ @  @
*  &&&   &     &   &    |/   @     @ @ @   @
*
* This Promela code is translation of coloured Petri net from the file 'atmr3.cpn'
*/

#define FOR(i) do :: i == 0 -> break :: else -> i--;
#define ROF od

#define NONDETERM_TRANS if :: goto doTrans :: trans = 0 fi

#define CLR(I) i##I = 0;
#define CLR_ALL CLR(1); CLR(2); CLR(3); CLR(4); CLR(5); CLR(6); CLR(7); CLR(8); CLR(9); CLR(10); CLR(11); CLR(12); CLR(13); CLR(14); CLR(15); CLR(16); CLR(17); CLR(18); CLR(19); CLR(20); CLR(21); CLR(22); CLR(23); CLR(24); CLR(25); CLR(26); CLR(27); CLR(28); trans = 0; c_code {now.guard = now.bindingFail = now.evaluationFail = now.exprError = now.search = 0;}
#define CONTINUE CLR_ALL; goto mainLoop

c_decl {
	\#define atmr_msgs3_1_1_as_txt_cpn
	\#include "restrictions.h"
	\#ifndef MS_MAX_SIZE
	\#error "Please define MS_MAX_SIZE in restrictions.h"
	\#endif
	\#ifndef LIST_MAX_LENGTH
	\#error "Please define LIST_MAX_LENGTH in restrictions.h"
	\#endif
	\#ifndef COUNTER_MAX_VALUE
	\#error "Please define COUNTER_MAX_VALUE in restrictions.h"
	\#endif
	
	typedef short inttype;
	typedef unsigned char enumtype;
	
	\#include <stdio.h>
	
	\#include "include/ms.h"
	\#include "include/list.h"
	\#include "include/timed.h"
	
	void print_t(char *s) {
		static FILE *print_file = NULL;
		if (print_file == NULL) {
			print_file = stderr;
		}
		fprintf(print_file, "CPN: %s\n", s);
	}
	
	void print_t_int(int v) {
		static FILE *print_file = NULL;
		if (print_file == NULL) {
			print_file = stderr;
		}
		fprintf(print_file, "CPN: %d\n", v);
	}
	
//0
	typedef inttype cs_INT;
	void print_INT(cs_INT *a) {
		print_t_int(*a);
	}
	int cmp_INT(cs_INT *a, cs_INT *b) {
		return *a - *b;
	}
	void null_INT(cs_INT *a) {
		*a = 0;
	}
	GEN_MS_METHODS(INT)
	
//1
	typedef enumtype cs_BOOL;
	void print_BOOL(cs_BOOL *a) {
		print_t(*a ? "true" : "false");
	}
	int cmp_BOOL(cs_BOOL *a, cs_BOOL *b) {
		return *a && !*b || !*a && *b;
	}
	void null_BOOL(cs_BOOL *a) {
		*a = 0;
	}
	GEN_MS1_METHODS(BOOL, 2)
	GEN_MS_METHODS(BOOL)
	
//2
	typedef enumtype cs_type_1_unit;
	void print_type_1_unit(cs_type_1_unit *a) {
	}
	int cmp_type_1_unit(cs_type_1_unit *a, cs_type_1_unit *b) {
		return *a - *b;
	}
	void null_type_1_unit(cs_type_1_unit *a) {
		*a = 0;
	}
	GEN_MS1_METHODS(type_1_unit, 1)
	GEN_MS_METHODS(type_1_unit)
	
//3
	typedef struct cs_LOCALINT {
		cs_INT field_1;
		cs_INT field_2;
	} cs_LOCALINT;
	void print_LOCALINT(cs_LOCALINT *val) {
		print_t("(");
		print_INT(&val->field_1);
		print_INT(&val->field_2);
		print_t(")");
	}
	int cmp_LOCALINT(cs_LOCALINT*this, cs_LOCALINT*that) {
		int res;
		res = cmp_INT(&this->field_1, &that->field_1);
		if (res != 0) return res;
		res = cmp_INT(&this->field_2, &that->field_2);
		if (res != 0) return res;
	}
	void null_LOCALINT(cs_LOCALINT *val) {
		null_INT(&val->field_1);
		null_INT(&val->field_2);
	}
	GEN_MS_METHODS(LOCALINT)
	
//4
	GEN_LIST_METHODS(ARRAYINT, INT)
	GEN_MS_METHODS(ARRAYINT)
	
//5
	typedef struct cs_LOCALARRAYINT {
		cs_INT field_1;
		cs_ARRAYINT field_2;
	} cs_LOCALARRAYINT;
	void print_LOCALARRAYINT(cs_LOCALARRAYINT *val) {
		print_t("(");
		print_INT(&val->field_1);
		print_ARRAYINT(&val->field_2);
		print_t(")");
	}
	int cmp_LOCALARRAYINT(cs_LOCALARRAYINT*this, cs_LOCALARRAYINT*that) {
		int res;
		res = cmp_INT(&this->field_1, &that->field_1);
		if (res != 0) return res;
		res = cmp_ARRAYINT(&this->field_2, &that->field_2);
		if (res != 0) return res;
	}
	void null_LOCALARRAYINT(cs_LOCALARRAYINT *val) {
		null_INT(&val->field_1);
		null_ARRAYINT(&val->field_2);
	}
	GEN_MS_METHODS(LOCALARRAYINT)
	
//6
	typedef enumtype cs_SIGTYPE;
	void print_SIGTYPE(cs_SIGTYPE *a) {
		switch(*a) {
			case 0: print_t("D"); break;
			case 1: print_t("E"); break;
			case 2: print_t("R"); break;
		}
	}
	int cmp_SIGTYPE(cs_SIGTYPE *a, cs_SIGTYPE *b) {
		return *a - *b;
	}
	void null_SIGTYPE(cs_SIGTYPE *a) {
		*a = 0;
	}
	GEN_MS1_METHODS(SIGTYPE, 3)
	GEN_MS_METHODS(SIGTYPE)
	
//7
	typedef struct cs_SIGNAL {
		cs_INT field_1;
		cs_INT field_2;
		cs_SIGTYPE field_3;
		cs_INT field_4;
	} cs_SIGNAL;
	void print_SIGNAL(cs_SIGNAL *val) {
		print_t("(");
		print_INT(&val->field_1);
		print_INT(&val->field_2);
		print_SIGTYPE(&val->field_3);
		print_INT(&val->field_4);
		print_t(")");
	}
	int cmp_SIGNAL(cs_SIGNAL*this, cs_SIGNAL*that) {
		int res;
		res = cmp_INT(&this->field_1, &that->field_1);
		if (res != 0) return res;
		res = cmp_INT(&this->field_2, &that->field_2);
		if (res != 0) return res;
		res = cmp_SIGTYPE(&this->field_3, &that->field_3);
		if (res != 0) return res;
		res = cmp_INT(&this->field_4, &that->field_4);
		if (res != 0) return res;
	}
	void null_SIGNAL(cs_SIGNAL *val) {
		null_INT(&val->field_1);
		null_INT(&val->field_2);
		null_SIGTYPE(&val->field_3);
		null_INT(&val->field_4);
	}
	GEN_MS_METHODS(SIGNAL)
	
//8
	typedef struct cs_type_2_P__ {
	} cs_type_2_P__;
	void print_type_2_P__(cs_type_2_P__ *val) {
		print_t("(");
		print_t(")");
	}
	int cmp_type_2_P__(cs_type_2_P__*this, cs_type_2_P__*that) {
		int res;
	}
	void null_type_2_P__(cs_type_2_P__ *val) {
	}
	GEN_MS_METHODS(type_2_P__)
	
//9
	typedef struct cs_type_3_P_int_ {
		cs_INT field_1;
	} cs_type_3_P_int_;
	void print_type_3_P_int_(cs_type_3_P_int_ *val) {
		print_t("(");
		print_INT(&val->field_1);
		print_t(")");
	}
	int cmp_type_3_P_int_(cs_type_3_P_int_*this, cs_type_3_P_int_*that) {
		int res;
		res = cmp_INT(&this->field_1, &that->field_1);
		if (res != 0) return res;
	}
	void null_type_3_P_int_(cs_type_3_P_int_ *val) {
		null_INT(&val->field_1);
	}
	GEN_MS_METHODS(type_3_P_int_)
	
//10
	typedef struct cs_type_4_P_int_int_E_D_E_R__ {
		cs_INT field_1;
		cs_INT field_2;
		cs_SIGTYPE field_3;
	} cs_type_4_P_int_int_E_D_E_R__;
	void print_type_4_P_int_int_E_D_E_R__(cs_type_4_P_int_int_E_D_E_R__ *val) {
		print_t("(");
		print_INT(&val->field_1);
		print_INT(&val->field_2);
		print_SIGTYPE(&val->field_3);
		print_t(")");
	}
	int cmp_type_4_P_int_int_E_D_E_R__(cs_type_4_P_int_int_E_D_E_R__*this, cs_type_4_P_int_int_E_D_E_R__*that) {
		int res;
		res = cmp_INT(&this->field_1, &that->field_1);
		if (res != 0) return res;
		res = cmp_INT(&this->field_2, &that->field_2);
		if (res != 0) return res;
		res = cmp_SIGTYPE(&this->field_3, &that->field_3);
		if (res != 0) return res;
	}
	void null_type_4_P_int_int_E_D_E_R__(cs_type_4_P_int_int_E_D_E_R__ *val) {
		null_INT(&val->field_1);
		null_INT(&val->field_2);
		null_SIGTYPE(&val->field_3);
	}
	GEN_MS_METHODS(type_4_P_int_int_E_D_E_R__)
	
	\#define true 1
	\#define false 0
	\#define UNIT 0
	
	int dbgp_i;
	int dbgtmp;
}

c_code {
	\#include "predicates.h"
	
	int nodes;
	
// CPN variables
	cs_ARRAYINT
		v_station_other;
	cs_BOOL
		v_true, v_false;
	cs_INT
		v_station_req, v_I_param, v_I_src, v_station_ack, v_NonDeterm0, v_station_maxmsgs, v_id, v_g_ba,
		v_station_msgs, v_station_c, v_station_next, v_g_dst, v_g_cnt, v_station_maxc;
	cs_SIGTYPE
		v_D, v_E, v_R, v_I_type;
	
// temporary variables
	cs_ARRAYINT
		tmp_ARRAYINT[100];
	cs_BOOL
		tmp_BOOL[100];
	cs_INT
		tmp_INT[100], fT1_in1, fT1_in_v1, fT1_out1, fT2_in1, fT2_in_v1, fT2_out1, fT3_in1, fT3_in_v1,
		fT3_in3, fT3_in_v3, fT3_in4, fT3_in_v4, fT3_out1, fT3_out3, fT3_out4, fT4_in1, fT4_in_v1, fT4_in3,
		fT4_in_v3, fT4_out1, fT4_out3, fT5_in1, fT5_in_v1, fT5_in3, fT5_in_v3, fT5_out1, fT5_out3,
		fT6_in1, fT6_in_v1, fT6_in3, fT6_in_v3, fT6_out1, fT6_out3, fT7_in1, fT7_in_v1, fT7_out1, fT8_in1,
		fT8_in_v1, fT8_in3, fT8_in_v3, fT8_in4, fT8_in_v4, fT8_in5, fT8_in_v5, fT8_out1, fT8_out3,
		fT8_out4, fT8_out5, fT9_in1, fT9_in_v1, fT9_in3, fT9_in_v3, fT9_in4, fT9_in_v4, fT9_out1,
		fT9_out3, fT9_out4, fT10_in1, fT10_in_v1, fT10_in3, fT10_in_v3, fT10_in4, fT10_in_v4, fT10_in5,
		fT10_in_v5, fT10_in6, fT10_in_v6, fT10_out1, fT10_out3, fT10_out4, fT10_out5, fT10_out6, fT11_in1,
		fT11_in_v1, fT11_out1, fT12_in1, fT12_in_v1, fT12_in3, fT12_in_v3, fT12_in4, fT12_in_v4,
		fT12_out1, fT12_out3, fT12_out4, fT13_in1, fT13_in_v1, fT13_in3, fT13_in_v3, fT13_out1, fT13_out3,
		fT14_in1, fT14_in_v1, fT14_out1, fT15_in1, fT15_in_v1, fT15_in3, fT15_in_v3, fT15_out1, fT15_out3,
		fT16_in1, fT16_in_v1, fT16_in3, fT16_in_v3, fT16_in4, fT16_in_v4, fT16_out1, fT16_out3, fT16_out4,
		fT17_in1, fT17_in_v1, fT17_in3, fT17_in_v3, fT17_out1, fT17_out3, fT18_in1, fT18_in_v1, fT18_in3,
		fT18_in_v3, fT18_out1, fT18_out3, fT19_in1, fT19_in_v1, fT19_in3, fT19_in_v3, fT19_out1,
		fT19_out3, fT20_in1, fT20_in_v1, fT20_out1, tmp_p1_station_reset, tmp_p2_station_wait,
		tmp_p3_station_send, tmp_p13_NonDeterm0, tmp_p14_g_ba, tmp_p15_g_dst, tmp_p16_g_cnt,
		tmp_p17_station_idle;
	cs_LOCALARRAYINT
		tmp_LOCALARRAYINT[100], fT10_in11, fT10_in_v11, fT10_out11, tmp_p9_station_other;
	cs_LOCALINT
		tmp_LOCALINT[100], fT1_in2, fT1_in_v2, fT1_in3, fT1_in_v3, fT1_out2, fT1_out3, fT2_in2, fT2_in_v2,
		fT2_in3, fT2_in_v3, fT2_out2, fT2_out3, fT3_in5, fT3_in_v5, fT3_in6, fT3_in_v6, fT3_in7,
		fT3_in_v7, fT3_in8, fT3_in_v8, fT3_in9, fT3_in_v9, fT3_out5, fT3_out6, fT3_out7, fT3_out8,
		fT3_out9, fT4_in4, fT4_in_v4, fT4_in5, fT4_in_v5, fT4_in6, fT4_in_v6, fT4_in7, fT4_in_v7, fT4_in8,
		fT4_in_v8, fT4_out4, fT4_out5, fT4_out6, fT4_out7, fT4_out8, fT5_in4, fT5_in_v4, fT5_in5,
		fT5_in_v5, fT5_in6, fT5_in_v6, fT5_in7, fT5_in_v7, fT5_in8, fT5_in_v8, fT5_out4, fT5_out5,
		fT5_out6, fT5_out7, fT5_out8, fT6_in4, fT6_in_v4, fT6_in5, fT6_in_v5, fT6_in6, fT6_in_v6, fT6_in7,
		fT6_in_v7, fT6_in8, fT6_in_v8, fT6_out4, fT6_out5, fT6_out6, fT6_out7, fT6_out8, fT7_in3,
		fT7_in_v3, fT7_in4, fT7_in_v4, fT7_in5, fT7_in_v5, fT7_in6, fT7_in_v6, fT7_in7, fT7_in_v7,
		fT7_in8, fT7_in_v8, fT7_in9, fT7_in_v9, fT7_out3, fT7_out4, fT7_out5, fT7_out6, fT7_out7,
		fT7_out8, fT7_out9, fT8_in6, fT8_in_v6, fT8_out6, fT9_in5, fT9_in_v5, fT9_out5, fT10_in7,
		fT10_in_v7, fT10_in8, fT10_in_v8, fT10_in9, fT10_in_v9, fT10_in10, fT10_in_v10, fT10_in12,
		fT10_in_v12, fT10_out7, fT10_out8, fT10_out9, fT10_out10, fT10_out12, fT11_in3, fT11_in_v3,
		fT11_in4, fT11_in_v4, fT11_in5, fT11_in_v5, fT11_out3, fT11_out4, fT11_out5, fT12_in5, fT12_in_v5,
		fT12_out5, fT13_in4, fT13_in_v4, fT13_out4, fT14_in3, fT14_in_v3, fT14_out3, fT15_in4, fT15_in_v4,
		fT15_in5, fT15_in_v5, fT15_in6, fT15_in_v6, fT15_out4, fT15_out5, fT15_out6, fT16_in5, fT16_in_v5,
		fT16_out5, fT17_in4, fT17_in_v4, fT17_out4, fT18_in4, fT18_in_v4, fT18_out4, fT19_in4, fT19_in_v4,
		fT19_out4, fT20_in3, fT20_in_v3, fT20_in4, fT20_in_v4, fT20_in5, fT20_in_v5, fT20_out3, fT20_out4,
		fT20_out5, tmp_p5_station_req, tmp_p6_station_msgs, tmp_p7_station_c, tmp_p8_station_next,
		tmp_p10_station_ack, tmp_p11_station_maxc, tmp_p12_station_maxmsgs;
	cs_SIGNAL
		tmp_SIGNAL[100], fT3_in2, fT3_in_v2, fT3_out2, fT4_in2, fT4_in_v2, fT4_out2, fT5_in2, fT5_in_v2,
		fT5_out2, fT6_in2, fT6_in_v2, fT6_out2, fT7_in2, fT7_in_v2, fT7_out2, fT8_in2, fT8_in_v2,
		fT8_out2, fT9_in2, fT9_in_v2, fT9_out2, fT10_in2, fT10_in_v2, fT10_out2, fT11_in2, fT11_in_v2,
		fT11_out2, fT12_in2, fT12_in_v2, fT12_out2, fT13_in2, fT13_in_v2, fT13_out2, fT14_in2, fT14_in_v2,
		fT14_out2, fT15_in2, fT15_in_v2, fT15_out2, fT16_in2, fT16_in_v2, fT16_out2, fT17_in2, fT17_in_v2,
		fT17_out2, fT18_in2, fT18_in_v2, fT18_out2, fT19_in2, fT19_in_v2, fT19_out2, fT20_in2, fT20_in_v2,
		fT20_out2, tmp_p4_env;
	cs_SIGTYPE
		tmp_SIGTYPE[100];
	cs_type_1_unit
		tmp_type_1_unit[100];
	cs_type_2_P__
		tmp_type_2_P__[100];
	cs_type_3_P_int_
		tmp_type_3_P_int_[100];
	cs_type_4_P_int_int_E_D_E_R__
		tmp_type_4_P_int_int_E_D_E_R__[100];
	msOfARRAYINT
		tmp_msOfARRAYINT[100];
	msOfBOOL
		tmp_msOfBOOL[100];
	msOfINT
		tmp_msOfINT[100], r_p1_station_reset, r_p2_station_wait, r_p3_station_send, r_p13_NonDeterm0,
		r_p17_station_idle;
	msOfLOCALARRAYINT
		tmp_msOfLOCALARRAYINT[100], r_p9_station_other;
	msOfLOCALINT
		tmp_msOfLOCALINT[100], r_p5_station_req, r_p6_station_msgs, r_p7_station_c, r_p8_station_next,
		r_p10_station_ack, r_p11_station_maxc, r_p12_station_maxmsgs;
	msOfSIGNAL
		tmp_msOfSIGNAL[100];
	msOfSIGTYPE
		tmp_msOfSIGTYPE[100];
	msOftype_1_unit
		tmp_msOftype_1_unit[100];
	msOftype_2_P__
		tmp_msOftype_2_P__[100];
	msOftype_3_P_int_
		tmp_msOftype_3_P_int_[100];
	msOftype_4_P_int_int_E_D_E_R__
		tmp_msOftype_4_P_int_int_E_D_E_R__[100];
	
	int i, j;
	inttype timeNext;
	int msUsed[MS_MAX_SIZE], ms1Count[3];
}

c_state "int guard" "Global"
c_state "int bindingFail" "Global"
c_state "int evaluationFail" "Global"
c_state "int exprError" "Global"
c_state "int search" "Global"
byte final;
byte trans;
byte i1;
byte i2;
byte i3;
byte i4;
byte i5;
byte i6;
byte i7;
byte i8;
byte i9;
byte i10;
byte i11;
byte i12;
byte i13;
byte i14;
byte i15;
byte i16;
byte i17;
byte i18;
byte i19;
byte i20;
byte i21;
byte i22;
byte i23;
byte i24;
byte i25;
byte i26;
byte i27;
byte i28;

c_code {
	\#include "user_functions.h"
}

// CPN places
c_state "msOfINT p1_station_reset" "Global"
c_state "msOfINT p2_station_wait" "Global"
c_state "msOfINT p3_station_send" "Global"
c_state "cs_SIGNAL p4_env" "Global"
c_state "msOfLOCALINT p5_station_req" "Global"
c_state "msOfLOCALINT p6_station_msgs" "Global"
c_state "msOfLOCALINT p7_station_c" "Global"
c_state "msOfLOCALINT p8_station_next" "Global"
c_state "msOfLOCALARRAYINT p9_station_other" "Global"
c_state "msOfLOCALINT p10_station_ack" "Global"
c_state "msOfLOCALINT p11_station_maxc" "Global"
c_state "msOfLOCALINT p12_station_maxmsgs" "Global"
c_state "msOfINT p13_NonDeterm0" "Global"
c_state "cs_INT p14_g_ba" "Global"
c_state "cs_INT p15_g_dst" "Global"
c_state "cs_INT p16_g_cnt" "Global"
c_state "msOfINT p17_station_idle" "Global"

active proctype cpn() {
initializing:
	c_code {
#define EPRINT(INTVAL) printf("	%s = %d\n", #INTVAL, INTVAL);
		EPRINT(MS_MAX_SIZE)
		EPRINT(LIST_MAX_LENGTH)
		EPRINT(COUNTER_MAX_VALUE)
// constants
		v_D = 0;
		v_E = 1;
		v_true = 1;
		v_R = 2;
		v_false = 0;
		
// place "p1_station_reset" initial marking
		empty_msOfINT(&now.p1_station_reset);
		
// place "p2_station_wait" initial marking
		empty_msOfINT(&now.p2_station_wait);
		
// place "p3_station_send" initial marking
		empty_msOfINT(&now.p3_station_send);
		
// place "p4_env" initial marking
		tmp_INT[0] = 0;
		tmp_p4_env.field_1 = tmp_INT[0];
		tmp_INT[1] = 1;
		tmp_p4_env.field_2 = tmp_INT[1];
		tmp_SIGTYPE[2] = v_E;
		tmp_p4_env.field_3 = tmp_SIGTYPE[2];
		tmp_INT[3] = 0;
		tmp_p4_env.field_4 = tmp_INT[3];
		now.p4_env = tmp_p4_env;
		
// place "p5_station_req" initial marking
		empty_msOfLOCALINT(&now.p5_station_req);
		tmp_INT[0] = 3;
		tmp_p5_station_req.field_1 = tmp_INT[0];
		tmp_INT[1] = 0;
		tmp_p5_station_req.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p5_station_req, &tmp_p5_station_req);
		tmp_INT[0] = 1;
		tmp_p5_station_req.field_1 = tmp_INT[0];
		tmp_INT[1] = 0;
		tmp_p5_station_req.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p5_station_req, &tmp_p5_station_req);
		tmp_INT[0] = 2;
		tmp_p5_station_req.field_1 = tmp_INT[0];
		tmp_INT[1] = 0;
		tmp_p5_station_req.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p5_station_req, &tmp_p5_station_req);
		
// place "p6_station_msgs" initial marking
		empty_msOfLOCALINT(&now.p6_station_msgs);
		tmp_INT[0] = 3;
		tmp_p6_station_msgs.field_1 = tmp_INT[0];
		tmp_INT[1] = 0;
		tmp_p6_station_msgs.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p6_station_msgs, &tmp_p6_station_msgs);
		tmp_INT[0] = 1;
		tmp_p6_station_msgs.field_1 = tmp_INT[0];
		tmp_INT[1] = 0;
		tmp_p6_station_msgs.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p6_station_msgs, &tmp_p6_station_msgs);
		tmp_INT[0] = 2;
		tmp_p6_station_msgs.field_1 = tmp_INT[0];
		tmp_INT[1] = 0;
		tmp_p6_station_msgs.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p6_station_msgs, &tmp_p6_station_msgs);
		
// place "p7_station_c" initial marking
		empty_msOfLOCALINT(&now.p7_station_c);
		tmp_INT[0] = 3;
		tmp_p7_station_c.field_1 = tmp_INT[0];
		tmp_INT[1] = 2;
		tmp_p7_station_c.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p7_station_c, &tmp_p7_station_c);
		tmp_INT[0] = 1;
		tmp_p7_station_c.field_1 = tmp_INT[0];
		tmp_INT[1] = 2;
		tmp_p7_station_c.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p7_station_c, &tmp_p7_station_c);
		tmp_INT[0] = 2;
		tmp_p7_station_c.field_1 = tmp_INT[0];
		tmp_INT[1] = 2;
		tmp_p7_station_c.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p7_station_c, &tmp_p7_station_c);
		
// place "p8_station_next" initial marking
		empty_msOfLOCALINT(&now.p8_station_next);
		tmp_INT[0] = 3;
		tmp_p8_station_next.field_1 = tmp_INT[0];
		tmp_INT[1] = 1;
		tmp_p8_station_next.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p8_station_next, &tmp_p8_station_next);
		tmp_INT[0] = 1;
		tmp_p8_station_next.field_1 = tmp_INT[0];
		tmp_INT[1] = 2;
		tmp_p8_station_next.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p8_station_next, &tmp_p8_station_next);
		tmp_INT[0] = 2;
		tmp_p8_station_next.field_1 = tmp_INT[0];
		tmp_INT[1] = 3;
		tmp_p8_station_next.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p8_station_next, &tmp_p8_station_next);
		
// place "p9_station_other" initial marking
		empty_msOfLOCALARRAYINT(&now.p9_station_other);
		tmp_INT[0] = 3;
		tmp_p9_station_other.field_1 = tmp_INT[0];
		tmp_INT[2] = 1;
		tmp_ARRAYINT[1].content[0] = tmp_INT[2];
		tmp_INT[3] = 2;
		tmp_ARRAYINT[1].content[1] = tmp_INT[3];
		tmp_ARRAYINT[1].length = 2;
		tmp_p9_station_other.field_2 = tmp_ARRAYINT[1];
		put_msOfLOCALARRAYINT(&now.p9_station_other, &tmp_p9_station_other);
		tmp_INT[0] = 1;
		tmp_p9_station_other.field_1 = tmp_INT[0];
		tmp_INT[2] = 2;
		tmp_ARRAYINT[1].content[0] = tmp_INT[2];
		tmp_INT[3] = 3;
		tmp_ARRAYINT[1].content[1] = tmp_INT[3];
		tmp_ARRAYINT[1].length = 2;
		tmp_p9_station_other.field_2 = tmp_ARRAYINT[1];
		put_msOfLOCALARRAYINT(&now.p9_station_other, &tmp_p9_station_other);
		tmp_INT[0] = 2;
		tmp_p9_station_other.field_1 = tmp_INT[0];
		tmp_INT[2] = 1;
		tmp_ARRAYINT[1].content[0] = tmp_INT[2];
		tmp_INT[3] = 3;
		tmp_ARRAYINT[1].content[1] = tmp_INT[3];
		tmp_ARRAYINT[1].length = 2;
		tmp_p9_station_other.field_2 = tmp_ARRAYINT[1];
		put_msOfLOCALARRAYINT(&now.p9_station_other, &tmp_p9_station_other);
		
// place "p10_station_ack" initial marking
		empty_msOfLOCALINT(&now.p10_station_ack);
		tmp_INT[0] = 3;
		tmp_p10_station_ack.field_1 = tmp_INT[0];
		tmp_INT[1] = 0;
		tmp_p10_station_ack.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p10_station_ack, &tmp_p10_station_ack);
		tmp_INT[0] = 1;
		tmp_p10_station_ack.field_1 = tmp_INT[0];
		tmp_INT[1] = 0;
		tmp_p10_station_ack.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p10_station_ack, &tmp_p10_station_ack);
		tmp_INT[0] = 2;
		tmp_p10_station_ack.field_1 = tmp_INT[0];
		tmp_INT[1] = 0;
		tmp_p10_station_ack.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p10_station_ack, &tmp_p10_station_ack);
		
// place "p11_station_maxc" initial marking
		empty_msOfLOCALINT(&now.p11_station_maxc);
		tmp_INT[0] = 3;
		tmp_p11_station_maxc.field_1 = tmp_INT[0];
		tmp_INT[1] = 2;
		tmp_p11_station_maxc.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p11_station_maxc, &tmp_p11_station_maxc);
		tmp_INT[0] = 1;
		tmp_p11_station_maxc.field_1 = tmp_INT[0];
		tmp_INT[1] = 2;
		tmp_p11_station_maxc.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p11_station_maxc, &tmp_p11_station_maxc);
		tmp_INT[0] = 2;
		tmp_p11_station_maxc.field_1 = tmp_INT[0];
		tmp_INT[1] = 2;
		tmp_p11_station_maxc.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p11_station_maxc, &tmp_p11_station_maxc);
		
// place "p12_station_maxmsgs" initial marking
		empty_msOfLOCALINT(&now.p12_station_maxmsgs);
		tmp_INT[0] = 3;
		tmp_p12_station_maxmsgs.field_1 = tmp_INT[0];
		tmp_INT[1] = 3;
		tmp_p12_station_maxmsgs.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p12_station_maxmsgs, &tmp_p12_station_maxmsgs);
		tmp_INT[0] = 1;
		tmp_p12_station_maxmsgs.field_1 = tmp_INT[0];
		tmp_INT[1] = 3;
		tmp_p12_station_maxmsgs.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p12_station_maxmsgs, &tmp_p12_station_maxmsgs);
		tmp_INT[0] = 2;
		tmp_p12_station_maxmsgs.field_1 = tmp_INT[0];
		tmp_INT[1] = 3;
		tmp_p12_station_maxmsgs.field_2 = tmp_INT[1];
		put_msOfLOCALINT(&now.p12_station_maxmsgs, &tmp_p12_station_maxmsgs);
		
// place "p13_NonDeterm0" initial marking
		empty_msOfINT(&now.p13_NonDeterm0);
		tmp_p13_NonDeterm0 = 0;
		put_msOfINT(&now.p13_NonDeterm0, &tmp_p13_NonDeterm0);
		tmp_p13_NonDeterm0 = 1;
		put_msOfINT(&now.p13_NonDeterm0, &tmp_p13_NonDeterm0);
		
// place "p14_g_ba" initial marking
		tmp_p14_g_ba = 0;
		now.p14_g_ba = tmp_p14_g_ba;
		
// place "p15_g_dst" initial marking
		tmp_p15_g_dst = 0;
		now.p15_g_dst = tmp_p15_g_dst;
		
// place "p16_g_cnt" initial marking
		tmp_p16_g_cnt = 0;
		now.p16_g_cnt = tmp_p16_g_cnt;
		
// place "p17_station_idle" initial marking
		empty_msOfINT(&now.p17_station_idle);
		tmp_p17_station_idle = 3;
		put_msOfINT(&now.p17_station_idle, &tmp_p17_station_idle);
		tmp_p17_station_idle = 1;
		put_msOfINT(&now.p17_station_idle, &tmp_p17_station_idle);
		tmp_p17_station_idle = 2;
		put_msOfINT(&now.p17_station_idle, &tmp_p17_station_idle);
		
		printf("\n");
	} ;
	
mainLoop:
	atomic {
		assert( c_expr { valid_state() } );
		final = 1;
mainLoopInner:
		c_code { printf("\r---> %d", ++nodes); } ;
// transition #1 "t1_i__s" enabled binding search
		c_code { now.i1 = now.p17_station_idle.size; } ;
		FOR (i1)
			c_code { now.i2 = now.p7_station_c.size; } ;
			FOR (i2)
				c_code { now.i3 = now.p6_station_msgs.size; } ;
				FOR (i3)
					c_code {
						;
						copy_msOfINT(&now.p17_station_idle, &fT1_in1, now.i1);
						v_id = fT1_in1;
						copy_msOfLOCALINT(&now.p7_station_c, &fT1_in2, now.i2);
						v_station_c = fT1_in2.field_2;
						copy_msOfLOCALINT(&now.p6_station_msgs, &fT1_in3, now.i3);
						v_station_msgs = fT1_in3.field_2;
					} ;
					if
					:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
					:: else ->
						
						c_code {
							now.evaluationFail = now.exprError = false;
							tmp_INT[1] = 0;
							tmp_INT[2] = v_station_msgs;
							tmp_BOOL[0] = tmp_INT[1] < tmp_INT[2];
							tmp_INT[4] = 0;
							tmp_INT[5] = v_station_c;
							tmp_BOOL[3] = tmp_INT[4] < tmp_INT[5];
							now.guard = tmp_BOOL[0] && tmp_BOOL[3];
							if (! now.guard) {
								goto t1_check;
							}
							fT1_in_v1 = v_id;
							if (cmp_INT(&fT1_in1, &fT1_in_v1)) {
								now.guard = false;
								goto t1_check;
							}
							tmp_INT[0] = v_id;
							fT1_in_v2.field_1 = tmp_INT[0];
							tmp_INT[1] = v_station_c;
							fT1_in_v2.field_2 = tmp_INT[1];
							if (cmp_LOCALINT(&fT1_in2, &fT1_in_v2)) {
								now.guard = false;
								goto t1_check;
							}
							r_p7_station_c.size = 0;
							for (i = 0; i < now.p7_station_c.size; ++i) {
								if (i != now.i2) {
									put_msOfLOCALINT(&r_p7_station_c, &now.p7_station_c.content[i]);
								}
							}
							tmp_INT[0] = v_id;
							fT1_in_v3.field_1 = tmp_INT[0];
							tmp_INT[1] = v_station_msgs;
							fT1_in_v3.field_2 = tmp_INT[1];
							if (cmp_LOCALINT(&fT1_in3, &fT1_in_v3)) {
								now.guard = false;
								goto t1_check;
							}
							r_p6_station_msgs.size = 0;
							for (i = 0; i < now.p6_station_msgs.size; ++i) {
								if (i != now.i3) {
									put_msOfLOCALINT(&r_p6_station_msgs, &now.p6_station_msgs.content[i]);
								}
							}
							fT1_out1 = v_id;
							if (now.p3_station_send.size + 1 > MS_MAX_SIZE) {
								now.evaluationFail = true;
								goto t1_check;
							}
							tmp_INT[0] = v_id;
							fT1_out2.field_1 = tmp_INT[0];
							tmp_INT[1] = v_station_c;
							fT1_out2.field_2 = tmp_INT[1];
							if (r_p7_station_c.size + 1 > MS_MAX_SIZE) {
								now.evaluationFail = true;
								goto t1_check;
							}
							tmp_INT[0] = v_id;
							fT1_out3.field_1 = tmp_INT[0];
							tmp_INT[1] = v_station_msgs;
							fT1_out3.field_2 = tmp_INT[1];
							if (r_p6_station_msgs.size + 1 > MS_MAX_SIZE) {
								now.evaluationFail = true;
								goto t1_check;
							}
t1_check:
							if (now.evaluationFail) now.guard = false;
						} ;
						assert( c_expr { ! now.evaluationFail } );
						if
						:: c_expr { now.guard } ->
							final = 0;
							trans = 1;
							NONDETERM_TRANS
						:: else -> skip
						fi
					fi
				ROF
			ROF
		ROF
		;
		
// transition #2 "t2_i__w" enabled binding search
		c_code { now.i1 = now.p17_station_idle.size; } ;
		FOR (i1)
			c_code { now.i2 = now.p7_station_c.size; } ;
			FOR (i2)
				c_code { now.i3 = now.p6_station_msgs.size; } ;
				FOR (i3)
					c_code {
						;
						copy_msOfINT(&now.p17_station_idle, &fT2_in1, now.i1);
						v_id = fT2_in1;
						copy_msOfLOCALINT(&now.p7_station_c, &fT2_in2, now.i2);
						v_station_c = fT2_in2.field_2;
						copy_msOfLOCALINT(&now.p6_station_msgs, &fT2_in3, now.i3);
						v_station_msgs = fT2_in3.field_2;
					} ;
					if
					:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
					:: else ->
						
						c_code {
							now.evaluationFail = now.exprError = false;
							tmp_INT[1] = 0;
							tmp_INT[2] = v_station_msgs;
							tmp_BOOL[0] = tmp_INT[1] < tmp_INT[2];
							tmp_INT[4] = 0;
							tmp_INT[5] = v_station_c;
							tmp_BOOL[3] = ! cmp_INT(&tmp_INT[4], &tmp_INT[5]);
							now.guard = tmp_BOOL[0] && tmp_BOOL[3];
							if (! now.guard) {
								goto t2_check;
							}
							fT2_in_v1 = v_id;
							if (cmp_INT(&fT2_in1, &fT2_in_v1)) {
								now.guard = false;
								goto t2_check;
							}
							tmp_INT[0] = v_id;
							fT2_in_v2.field_1 = tmp_INT[0];
							tmp_INT[1] = v_station_c;
							fT2_in_v2.field_2 = tmp_INT[1];
							if (cmp_LOCALINT(&fT2_in2, &fT2_in_v2)) {
								now.guard = false;
								goto t2_check;
							}
							r_p7_station_c.size = 0;
							for (i = 0; i < now.p7_station_c.size; ++i) {
								if (i != now.i2) {
									put_msOfLOCALINT(&r_p7_station_c, &now.p7_station_c.content[i]);
								}
							}
							tmp_INT[0] = v_id;
							fT2_in_v3.field_1 = tmp_INT[0];
							tmp_INT[1] = v_station_msgs;
							fT2_in_v3.field_2 = tmp_INT[1];
							if (cmp_LOCALINT(&fT2_in3, &fT2_in_v3)) {
								now.guard = false;
								goto t2_check;
							}
							r_p6_station_msgs.size = 0;
							for (i = 0; i < now.p6_station_msgs.size; ++i) {
								if (i != now.i3) {
									put_msOfLOCALINT(&r_p6_station_msgs, &now.p6_station_msgs.content[i]);
								}
							}
							fT2_out1 = v_id;
							if (now.p2_station_wait.size + 1 > MS_MAX_SIZE) {
								now.evaluationFail = true;
								goto t2_check;
							}
							tmp_INT[0] = v_id;
							fT2_out2.field_1 = tmp_INT[0];
							tmp_INT[1] = v_station_c;
							fT2_out2.field_2 = tmp_INT[1];
							if (r_p7_station_c.size + 1 > MS_MAX_SIZE) {
								now.evaluationFail = true;
								goto t2_check;
							}
							tmp_INT[0] = v_id;
							fT2_out3.field_1 = tmp_INT[0];
							tmp_INT[1] = v_station_msgs;
							fT2_out3.field_2 = tmp_INT[1];
							if (r_p6_station_msgs.size + 1 > MS_MAX_SIZE) {
								now.evaluationFail = true;
								goto t2_check;
							}
t2_check:
							if (now.evaluationFail) now.guard = false;
						} ;
						assert( c_expr { ! now.evaluationFail } );
						if
						:: c_expr { now.guard } ->
							final = 0;
							trans = 2;
							NONDETERM_TRANS
						:: else -> skip
						fi
					fi
				ROF
			ROF
		ROF
		;
		
// transition #3 "t3_id__" enabled binding search
		c_code { now.i1 = now.p17_station_idle.size; } ;
		FOR (i1)
			c_code { now.i5 = now.p10_station_ack.size; } ;
			FOR (i5)
				c_code { now.i6 = now.p12_station_maxmsgs.size; } ;
				FOR (i6)
					c_code { now.i7 = now.p6_station_msgs.size; } ;
					FOR (i7)
						c_code { now.i8 = now.p8_station_next.size; } ;
						FOR (i8)
							c_code { now.i9 = now.p5_station_req.size; } ;
							FOR (i9)
								c_code {
									;
									copy_msOfINT(&now.p17_station_idle, &fT3_in1, now.i1);
									v_id = fT3_in1;
									fT3_in2 = now.p4_env;
									v_I_src = fT3_in2.field_1;
									v_I_type = fT3_in2.field_3;
									v_I_param = fT3_in2.field_4;
									fT3_in3 = now.p16_g_cnt;
									v_g_cnt = fT3_in3;
									fT3_in4 = now.p15_g_dst;
									v_g_dst = fT3_in4;
									copy_msOfLOCALINT(&now.p10_station_ack, &fT3_in5, now.i5);
									v_station_ack = fT3_in5.field_2;
									copy_msOfLOCALINT(&now.p12_station_maxmsgs, &fT3_in6, now.i6);
									v_station_maxmsgs = fT3_in6.field_2;
									copy_msOfLOCALINT(&now.p6_station_msgs, &fT3_in7, now.i7);
									v_station_msgs = fT3_in7.field_2;
									copy_msOfLOCALINT(&now.p8_station_next, &fT3_in8, now.i8);
									v_station_next = fT3_in8.field_2;
									copy_msOfLOCALINT(&now.p5_station_req, &fT3_in9, now.i9);
									v_station_req = fT3_in9.field_2;
								} ;
								if
								:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
								:: else ->
									
									c_code {
										now.evaluationFail = now.exprError = false;
										tmp_INT[2] = 0;
										tmp_INT[3] = v_station_msgs;
										tmp_BOOL[1] = ! cmp_INT(&tmp_INT[2], &tmp_INT[3]);
										tmp_SIGTYPE[5] = v_D;
										tmp_SIGTYPE[6] = v_I_type;
										tmp_BOOL[4] = ! cmp_SIGTYPE(&tmp_SIGTYPE[5], &tmp_SIGTYPE[6]);
										tmp_BOOL[0] = tmp_BOOL[1] && tmp_BOOL[4];
										tmp_INT[8] = v_id;
										tmp_INT[9] = v_g_dst;
										tmp_BOOL[7] = ! cmp_INT(&tmp_INT[8], &tmp_INT[9]);
										now.guard = tmp_BOOL[0] && tmp_BOOL[7];
										if (! now.guard) {
											goto t3_check;
										}
										fT3_in_v1 = v_id;
										if (cmp_INT(&fT3_in1, &fT3_in_v1)) {
											now.guard = false;
											goto t3_check;
										}
										r_p17_station_idle.size = 0;
										for (i = 0; i < now.p17_station_idle.size; ++i) {
											if (i != now.i1) {
												put_msOfINT(&r_p17_station_idle, &now.p17_station_idle.content[i]);
											}
										}
										tmp_INT[0] = v_I_src;
										fT3_in_v2.field_1 = tmp_INT[0];
										tmp_INT[1] = v_id;
										fT3_in_v2.field_2 = tmp_INT[1];
										tmp_SIGTYPE[2] = v_I_type;
										fT3_in_v2.field_3 = tmp_SIGTYPE[2];
										tmp_INT[3] = v_I_param;
										fT3_in_v2.field_4 = tmp_INT[3];
										if (cmp_SIGNAL(&fT3_in2, &fT3_in_v2)) {
											now.guard = false;
											goto t3_check;
										}
										fT3_in_v3 = v_g_cnt;
										if (cmp_INT(&fT3_in3, &fT3_in_v3)) {
											now.guard = false;
											goto t3_check;
										}
										fT3_in_v4 = v_g_dst;
										if (cmp_INT(&fT3_in4, &fT3_in_v4)) {
											now.guard = false;
											goto t3_check;
										}
										tmp_INT[0] = v_id;
										fT3_in_v5.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_ack;
										fT3_in_v5.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT3_in5, &fT3_in_v5)) {
											now.guard = false;
											goto t3_check;
										}
										r_p10_station_ack.size = 0;
										for (i = 0; i < now.p10_station_ack.size; ++i) {
											if (i != now.i5) {
												put_msOfLOCALINT(&r_p10_station_ack, &now.p10_station_ack.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT3_in_v6.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_maxmsgs;
										fT3_in_v6.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT3_in6, &fT3_in_v6)) {
											now.guard = false;
											goto t3_check;
										}
										r_p12_station_maxmsgs.size = 0;
										for (i = 0; i < now.p12_station_maxmsgs.size; ++i) {
											if (i != now.i6) {
												put_msOfLOCALINT(&r_p12_station_maxmsgs, &now.p12_station_maxmsgs.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT3_in_v7.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_msgs;
										fT3_in_v7.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT3_in7, &fT3_in_v7)) {
											now.guard = false;
											goto t3_check;
										}
										r_p6_station_msgs.size = 0;
										for (i = 0; i < now.p6_station_msgs.size; ++i) {
											if (i != now.i7) {
												put_msOfLOCALINT(&r_p6_station_msgs, &now.p6_station_msgs.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT3_in_v8.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_next;
										fT3_in_v8.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT3_in8, &fT3_in_v8)) {
											now.guard = false;
											goto t3_check;
										}
										r_p8_station_next.size = 0;
										for (i = 0; i < now.p8_station_next.size; ++i) {
											if (i != now.i8) {
												put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT3_in_v9.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_req;
										fT3_in_v9.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT3_in9, &fT3_in_v9)) {
											now.guard = false;
											goto t3_check;
										}
										r_p5_station_req.size = 0;
										for (i = 0; i < now.p5_station_req.size; ++i) {
											if (i != now.i9) {
												put_msOfLOCALINT(&r_p5_station_req, &now.p5_station_req.content[i]);
											}
										}
										fT3_out1 = v_id;
										if (r_p17_station_idle.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t3_check;
										}
										tmp_INT[0] = v_id;
										fT3_out2.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_next;
										fT3_out2.field_2 = tmp_INT[1];
										tmp_SIGTYPE[2] = v_E;
										fT3_out2.field_3 = tmp_SIGTYPE[2];
										tmp_INT[3] = 0;
										fT3_out2.field_4 = tmp_INT[3];
										tmp_INT[0] = v_g_cnt;
										tmp_INT[1] = 1;
										fT3_out3 = tmp_INT[0] - tmp_INT[1];
										fT3_out4 = 0;
										tmp_INT[0] = v_id;
										fT3_out5.field_1 = tmp_INT[0];
										tmp_INT[1] = 0;
										fT3_out5.field_2 = tmp_INT[1];
										if (r_p10_station_ack.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t3_check;
										}
										tmp_INT[0] = v_id;
										fT3_out6.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_maxmsgs;
										fT3_out6.field_2 = tmp_INT[1];
										if (r_p12_station_maxmsgs.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t3_check;
										}
										tmp_INT[0] = v_id;
										fT3_out7.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_maxmsgs;
										fT3_out7.field_2 = tmp_INT[1];
										if (r_p6_station_msgs.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t3_check;
										}
										tmp_INT[0] = v_id;
										fT3_out8.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_next;
										fT3_out8.field_2 = tmp_INT[1];
										if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t3_check;
										}
										tmp_INT[0] = v_id;
										fT3_out9.field_1 = tmp_INT[0];
										tmp_INT[1] = 1;
										fT3_out9.field_2 = tmp_INT[1];
										if (r_p5_station_req.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t3_check;
										}
t3_check:
										if (now.evaluationFail) now.guard = false;
									} ;
									assert( c_expr { ! now.evaluationFail } );
									if
									:: c_expr { now.guard } ->
										final = 0;
										trans = 3;
										NONDETERM_TRANS
									:: else -> skip
									fi
								fi
							ROF
						ROF
					ROF
				ROF
			ROF
		ROF
		;
		
// transition #4 "t4_idn" enabled binding search
		c_code { now.i1 = now.p17_station_idle.size; } ;
		FOR (i1)
			c_code { now.i4 = now.p10_station_ack.size; } ;
			FOR (i4)
				c_code { now.i5 = now.p12_station_maxmsgs.size; } ;
				FOR (i5)
					c_code { now.i6 = now.p6_station_msgs.size; } ;
					FOR (i6)
						c_code { now.i7 = now.p8_station_next.size; } ;
						FOR (i7)
							c_code { now.i8 = now.p5_station_req.size; } ;
							FOR (i8)
								c_code {
									;
									copy_msOfINT(&now.p17_station_idle, &fT4_in1, now.i1);
									v_id = fT4_in1;
									fT4_in2 = now.p4_env;
									v_I_src = fT4_in2.field_1;
									v_I_type = fT4_in2.field_3;
									v_I_param = fT4_in2.field_4;
									fT4_in3 = now.p15_g_dst;
									v_g_dst = fT4_in3;
									copy_msOfLOCALINT(&now.p10_station_ack, &fT4_in4, now.i4);
									v_station_ack = fT4_in4.field_2;
									copy_msOfLOCALINT(&now.p12_station_maxmsgs, &fT4_in5, now.i5);
									v_station_maxmsgs = fT4_in5.field_2;
									copy_msOfLOCALINT(&now.p6_station_msgs, &fT4_in6, now.i6);
									v_station_msgs = fT4_in6.field_2;
									copy_msOfLOCALINT(&now.p8_station_next, &fT4_in7, now.i7);
									v_station_next = fT4_in7.field_2;
									copy_msOfLOCALINT(&now.p5_station_req, &fT4_in8, now.i8);
									v_station_req = fT4_in8.field_2;
								} ;
								if
								:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
								:: else ->
									
									c_code {
										now.evaluationFail = now.exprError = false;
										tmp_INT[2] = 0;
										tmp_INT[3] = v_station_msgs;
										tmp_BOOL[1] = ! cmp_INT(&tmp_INT[2], &tmp_INT[3]);
										tmp_SIGTYPE[5] = v_D;
										tmp_SIGTYPE[6] = v_I_type;
										tmp_BOOL[4] = ! cmp_SIGTYPE(&tmp_SIGTYPE[5], &tmp_SIGTYPE[6]);
										tmp_BOOL[0] = tmp_BOOL[1] && tmp_BOOL[4];
										tmp_INT[8] = v_id;
										tmp_INT[9] = v_g_dst;
										tmp_BOOL[7] = cmp_INT(&tmp_INT[8], &tmp_INT[9]);
										now.guard = tmp_BOOL[0] && tmp_BOOL[7];
										if (! now.guard) {
											goto t4_check;
										}
										fT4_in_v1 = v_id;
										if (cmp_INT(&fT4_in1, &fT4_in_v1)) {
											now.guard = false;
											goto t4_check;
										}
										r_p17_station_idle.size = 0;
										for (i = 0; i < now.p17_station_idle.size; ++i) {
											if (i != now.i1) {
												put_msOfINT(&r_p17_station_idle, &now.p17_station_idle.content[i]);
											}
										}
										tmp_INT[0] = v_I_src;
										fT4_in_v2.field_1 = tmp_INT[0];
										tmp_INT[1] = v_id;
										fT4_in_v2.field_2 = tmp_INT[1];
										tmp_SIGTYPE[2] = v_I_type;
										fT4_in_v2.field_3 = tmp_SIGTYPE[2];
										tmp_INT[3] = v_I_param;
										fT4_in_v2.field_4 = tmp_INT[3];
										if (cmp_SIGNAL(&fT4_in2, &fT4_in_v2)) {
											now.guard = false;
											goto t4_check;
										}
										fT4_in_v3 = v_g_dst;
										if (cmp_INT(&fT4_in3, &fT4_in_v3)) {
											now.guard = false;
											goto t4_check;
										}
										tmp_INT[0] = v_id;
										fT4_in_v4.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_ack;
										fT4_in_v4.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT4_in4, &fT4_in_v4)) {
											now.guard = false;
											goto t4_check;
										}
										r_p10_station_ack.size = 0;
										for (i = 0; i < now.p10_station_ack.size; ++i) {
											if (i != now.i4) {
												put_msOfLOCALINT(&r_p10_station_ack, &now.p10_station_ack.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT4_in_v5.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_maxmsgs;
										fT4_in_v5.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT4_in5, &fT4_in_v5)) {
											now.guard = false;
											goto t4_check;
										}
										r_p12_station_maxmsgs.size = 0;
										for (i = 0; i < now.p12_station_maxmsgs.size; ++i) {
											if (i != now.i5) {
												put_msOfLOCALINT(&r_p12_station_maxmsgs, &now.p12_station_maxmsgs.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT4_in_v6.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_msgs;
										fT4_in_v6.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT4_in6, &fT4_in_v6)) {
											now.guard = false;
											goto t4_check;
										}
										r_p6_station_msgs.size = 0;
										for (i = 0; i < now.p6_station_msgs.size; ++i) {
											if (i != now.i6) {
												put_msOfLOCALINT(&r_p6_station_msgs, &now.p6_station_msgs.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT4_in_v7.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_next;
										fT4_in_v7.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT4_in7, &fT4_in_v7)) {
											now.guard = false;
											goto t4_check;
										}
										r_p8_station_next.size = 0;
										for (i = 0; i < now.p8_station_next.size; ++i) {
											if (i != now.i7) {
												put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT4_in_v8.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_req;
										fT4_in_v8.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT4_in8, &fT4_in_v8)) {
											now.guard = false;
											goto t4_check;
										}
										r_p5_station_req.size = 0;
										for (i = 0; i < now.p5_station_req.size; ++i) {
											if (i != now.i8) {
												put_msOfLOCALINT(&r_p5_station_req, &now.p5_station_req.content[i]);
											}
										}
										fT4_out1 = v_id;
										if (r_p17_station_idle.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t4_check;
										}
										tmp_INT[0] = v_id;
										fT4_out2.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_next;
										fT4_out2.field_2 = tmp_INT[1];
										tmp_SIGTYPE[2] = v_D;
										fT4_out2.field_3 = tmp_SIGTYPE[2];
										tmp_INT[3] = 0;
										fT4_out2.field_4 = tmp_INT[3];
										fT4_out3 = v_g_dst;
										tmp_INT[0] = v_id;
										fT4_out4.field_1 = tmp_INT[0];
										tmp_INT[1] = 0;
										fT4_out4.field_2 = tmp_INT[1];
										if (r_p10_station_ack.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t4_check;
										}
										tmp_INT[0] = v_id;
										fT4_out5.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_maxmsgs;
										fT4_out5.field_2 = tmp_INT[1];
										if (r_p12_station_maxmsgs.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t4_check;
										}
										tmp_INT[0] = v_id;
										fT4_out6.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_maxmsgs;
										fT4_out6.field_2 = tmp_INT[1];
										if (r_p6_station_msgs.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t4_check;
										}
										tmp_INT[0] = v_id;
										fT4_out7.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_next;
										fT4_out7.field_2 = tmp_INT[1];
										if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t4_check;
										}
										tmp_INT[0] = v_id;
										fT4_out8.field_1 = tmp_INT[0];
										tmp_INT[1] = 1;
										fT4_out8.field_2 = tmp_INT[1];
										if (r_p5_station_req.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t4_check;
										}
t4_check:
										if (now.evaluationFail) now.guard = false;
									} ;
									assert( c_expr { ! now.evaluationFail } );
									if
									:: c_expr { now.guard } ->
										final = 0;
										trans = 4;
										NONDETERM_TRANS
									:: else -> skip
									fi
								fi
							ROF
						ROF
					ROF
				ROF
			ROF
		ROF
		;
		
// transition #5 "t5_ie__" enabled binding search
		c_code { now.i1 = now.p17_station_idle.size; } ;
		FOR (i1)
			c_code { now.i4 = now.p10_station_ack.size; } ;
			FOR (i4)
				c_code { now.i5 = now.p12_station_maxmsgs.size; } ;
				FOR (i5)
					c_code { now.i6 = now.p6_station_msgs.size; } ;
					FOR (i6)
						c_code { now.i7 = now.p8_station_next.size; } ;
						FOR (i7)
							c_code { now.i8 = now.p5_station_req.size; } ;
							FOR (i8)
								c_code {
									;
									copy_msOfINT(&now.p17_station_idle, &fT5_in1, now.i1);
									v_id = fT5_in1;
									fT5_in2 = now.p4_env;
									v_I_src = fT5_in2.field_1;
									v_I_type = fT5_in2.field_3;
									v_I_param = fT5_in2.field_4;
									fT5_in3 = now.p14_g_ba;
									v_g_ba = fT5_in3;
									copy_msOfLOCALINT(&now.p10_station_ack, &fT5_in4, now.i4);
									v_station_ack = fT5_in4.field_2;
									copy_msOfLOCALINT(&now.p12_station_maxmsgs, &fT5_in5, now.i5);
									v_station_maxmsgs = fT5_in5.field_2;
									copy_msOfLOCALINT(&now.p6_station_msgs, &fT5_in6, now.i6);
									v_station_msgs = fT5_in6.field_2;
									copy_msOfLOCALINT(&now.p8_station_next, &fT5_in7, now.i7);
									v_station_next = fT5_in7.field_2;
									copy_msOfLOCALINT(&now.p5_station_req, &fT5_in8, now.i8);
									v_station_req = fT5_in8.field_2;
								} ;
								if
								:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
								:: else ->
									
									c_code {
										now.evaluationFail = now.exprError = false;
										tmp_INT[2] = 0;
										tmp_INT[3] = v_station_msgs;
										tmp_BOOL[1] = ! cmp_INT(&tmp_INT[2], &tmp_INT[3]);
										tmp_SIGTYPE[5] = v_E;
										tmp_SIGTYPE[6] = v_I_type;
										tmp_BOOL[4] = ! cmp_SIGTYPE(&tmp_SIGTYPE[5], &tmp_SIGTYPE[6]);
										tmp_BOOL[0] = tmp_BOOL[1] && tmp_BOOL[4];
										tmp_INT[8] = v_id;
										tmp_INT[9] = v_g_ba;
										tmp_BOOL[7] = ! cmp_INT(&tmp_INT[8], &tmp_INT[9]);
										now.guard = tmp_BOOL[0] && tmp_BOOL[7];
										if (! now.guard) {
											goto t5_check;
										}
										fT5_in_v1 = v_id;
										if (cmp_INT(&fT5_in1, &fT5_in_v1)) {
											now.guard = false;
											goto t5_check;
										}
										tmp_INT[0] = v_I_src;
										fT5_in_v2.field_1 = tmp_INT[0];
										tmp_INT[1] = v_id;
										fT5_in_v2.field_2 = tmp_INT[1];
										tmp_SIGTYPE[2] = v_I_type;
										fT5_in_v2.field_3 = tmp_SIGTYPE[2];
										tmp_INT[3] = v_I_param;
										fT5_in_v2.field_4 = tmp_INT[3];
										if (cmp_SIGNAL(&fT5_in2, &fT5_in_v2)) {
											now.guard = false;
											goto t5_check;
										}
										fT5_in_v3 = v_g_ba;
										if (cmp_INT(&fT5_in3, &fT5_in_v3)) {
											now.guard = false;
											goto t5_check;
										}
										tmp_INT[0] = v_id;
										fT5_in_v4.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_ack;
										fT5_in_v4.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT5_in4, &fT5_in_v4)) {
											now.guard = false;
											goto t5_check;
										}
										r_p10_station_ack.size = 0;
										for (i = 0; i < now.p10_station_ack.size; ++i) {
											if (i != now.i4) {
												put_msOfLOCALINT(&r_p10_station_ack, &now.p10_station_ack.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT5_in_v5.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_maxmsgs;
										fT5_in_v5.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT5_in5, &fT5_in_v5)) {
											now.guard = false;
											goto t5_check;
										}
										r_p12_station_maxmsgs.size = 0;
										for (i = 0; i < now.p12_station_maxmsgs.size; ++i) {
											if (i != now.i5) {
												put_msOfLOCALINT(&r_p12_station_maxmsgs, &now.p12_station_maxmsgs.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT5_in_v6.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_msgs;
										fT5_in_v6.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT5_in6, &fT5_in_v6)) {
											now.guard = false;
											goto t5_check;
										}
										r_p6_station_msgs.size = 0;
										for (i = 0; i < now.p6_station_msgs.size; ++i) {
											if (i != now.i6) {
												put_msOfLOCALINT(&r_p6_station_msgs, &now.p6_station_msgs.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT5_in_v7.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_next;
										fT5_in_v7.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT5_in7, &fT5_in_v7)) {
											now.guard = false;
											goto t5_check;
										}
										r_p8_station_next.size = 0;
										for (i = 0; i < now.p8_station_next.size; ++i) {
											if (i != now.i7) {
												put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT5_in_v8.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_req;
										fT5_in_v8.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT5_in8, &fT5_in_v8)) {
											now.guard = false;
											goto t5_check;
										}
										r_p5_station_req.size = 0;
										for (i = 0; i < now.p5_station_req.size; ++i) {
											if (i != now.i8) {
												put_msOfLOCALINT(&r_p5_station_req, &now.p5_station_req.content[i]);
											}
										}
										fT5_out1 = v_id;
										if (now.p1_station_reset.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t5_check;
										}
										tmp_INT[0] = v_id;
										fT5_out2.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_next;
										fT5_out2.field_2 = tmp_INT[1];
										tmp_SIGTYPE[2] = v_R;
										fT5_out2.field_3 = tmp_SIGTYPE[2];
										tmp_INT[3] = 0;
										fT5_out2.field_4 = tmp_INT[3];
										fT5_out3 = v_g_ba;
										tmp_INT[0] = v_id;
										fT5_out4.field_1 = tmp_INT[0];
										tmp_INT[1] = 0;
										fT5_out4.field_2 = tmp_INT[1];
										if (r_p10_station_ack.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t5_check;
										}
										tmp_INT[0] = v_id;
										fT5_out5.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_maxmsgs;
										fT5_out5.field_2 = tmp_INT[1];
										if (r_p12_station_maxmsgs.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t5_check;
										}
										tmp_INT[0] = v_id;
										fT5_out6.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_maxmsgs;
										fT5_out6.field_2 = tmp_INT[1];
										if (r_p6_station_msgs.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t5_check;
										}
										tmp_INT[0] = v_id;
										fT5_out7.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_next;
										fT5_out7.field_2 = tmp_INT[1];
										if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t5_check;
										}
										tmp_INT[0] = v_id;
										fT5_out8.field_1 = tmp_INT[0];
										tmp_INT[1] = 1;
										fT5_out8.field_2 = tmp_INT[1];
										if (r_p5_station_req.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t5_check;
										}
t5_check:
										if (now.evaluationFail) now.guard = false;
									} ;
									assert( c_expr { ! now.evaluationFail } );
									if
									:: c_expr { now.guard } ->
										final = 0;
										trans = 5;
										NONDETERM_TRANS
									:: else -> skip
									fi
								fi
							ROF
						ROF
					ROF
				ROF
			ROF
		ROF
		;
		
// transition #6 "t6_ien" enabled binding search
		c_code { now.i1 = now.p17_station_idle.size; } ;
		FOR (i1)
			c_code { now.i4 = now.p10_station_ack.size; } ;
			FOR (i4)
				c_code { now.i5 = now.p12_station_maxmsgs.size; } ;
				FOR (i5)
					c_code { now.i6 = now.p6_station_msgs.size; } ;
					FOR (i6)
						c_code { now.i7 = now.p8_station_next.size; } ;
						FOR (i7)
							c_code { now.i8 = now.p5_station_req.size; } ;
							FOR (i8)
								c_code {
									;
									copy_msOfINT(&now.p17_station_idle, &fT6_in1, now.i1);
									v_id = fT6_in1;
									fT6_in2 = now.p4_env;
									v_I_src = fT6_in2.field_1;
									v_I_type = fT6_in2.field_3;
									v_I_param = fT6_in2.field_4;
									fT6_in3 = now.p14_g_ba;
									v_g_ba = fT6_in3;
									copy_msOfLOCALINT(&now.p10_station_ack, &fT6_in4, now.i4);
									v_station_ack = fT6_in4.field_2;
									copy_msOfLOCALINT(&now.p12_station_maxmsgs, &fT6_in5, now.i5);
									v_station_maxmsgs = fT6_in5.field_2;
									copy_msOfLOCALINT(&now.p6_station_msgs, &fT6_in6, now.i6);
									v_station_msgs = fT6_in6.field_2;
									copy_msOfLOCALINT(&now.p8_station_next, &fT6_in7, now.i7);
									v_station_next = fT6_in7.field_2;
									copy_msOfLOCALINT(&now.p5_station_req, &fT6_in8, now.i8);
									v_station_req = fT6_in8.field_2;
								} ;
								if
								:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
								:: else ->
									
									c_code {
										now.evaluationFail = now.exprError = false;
										tmp_INT[2] = 0;
										tmp_INT[3] = v_station_msgs;
										tmp_BOOL[1] = ! cmp_INT(&tmp_INT[2], &tmp_INT[3]);
										tmp_SIGTYPE[5] = v_E;
										tmp_SIGTYPE[6] = v_I_type;
										tmp_BOOL[4] = ! cmp_SIGTYPE(&tmp_SIGTYPE[5], &tmp_SIGTYPE[6]);
										tmp_BOOL[0] = tmp_BOOL[1] && tmp_BOOL[4];
										tmp_INT[8] = v_id;
										tmp_INT[9] = v_g_ba;
										tmp_BOOL[7] = cmp_INT(&tmp_INT[8], &tmp_INT[9]);
										now.guard = tmp_BOOL[0] && tmp_BOOL[7];
										if (! now.guard) {
											goto t6_check;
										}
										fT6_in_v1 = v_id;
										if (cmp_INT(&fT6_in1, &fT6_in_v1)) {
											now.guard = false;
											goto t6_check;
										}
										r_p17_station_idle.size = 0;
										for (i = 0; i < now.p17_station_idle.size; ++i) {
											if (i != now.i1) {
												put_msOfINT(&r_p17_station_idle, &now.p17_station_idle.content[i]);
											}
										}
										tmp_INT[0] = v_I_src;
										fT6_in_v2.field_1 = tmp_INT[0];
										tmp_INT[1] = v_id;
										fT6_in_v2.field_2 = tmp_INT[1];
										tmp_SIGTYPE[2] = v_I_type;
										fT6_in_v2.field_3 = tmp_SIGTYPE[2];
										tmp_INT[3] = v_I_param;
										fT6_in_v2.field_4 = tmp_INT[3];
										if (cmp_SIGNAL(&fT6_in2, &fT6_in_v2)) {
											now.guard = false;
											goto t6_check;
										}
										fT6_in_v3 = v_g_ba;
										if (cmp_INT(&fT6_in3, &fT6_in_v3)) {
											now.guard = false;
											goto t6_check;
										}
										tmp_INT[0] = v_id;
										fT6_in_v4.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_ack;
										fT6_in_v4.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT6_in4, &fT6_in_v4)) {
											now.guard = false;
											goto t6_check;
										}
										r_p10_station_ack.size = 0;
										for (i = 0; i < now.p10_station_ack.size; ++i) {
											if (i != now.i4) {
												put_msOfLOCALINT(&r_p10_station_ack, &now.p10_station_ack.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT6_in_v5.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_maxmsgs;
										fT6_in_v5.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT6_in5, &fT6_in_v5)) {
											now.guard = false;
											goto t6_check;
										}
										r_p12_station_maxmsgs.size = 0;
										for (i = 0; i < now.p12_station_maxmsgs.size; ++i) {
											if (i != now.i5) {
												put_msOfLOCALINT(&r_p12_station_maxmsgs, &now.p12_station_maxmsgs.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT6_in_v6.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_msgs;
										fT6_in_v6.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT6_in6, &fT6_in_v6)) {
											now.guard = false;
											goto t6_check;
										}
										r_p6_station_msgs.size = 0;
										for (i = 0; i < now.p6_station_msgs.size; ++i) {
											if (i != now.i6) {
												put_msOfLOCALINT(&r_p6_station_msgs, &now.p6_station_msgs.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT6_in_v7.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_next;
										fT6_in_v7.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT6_in7, &fT6_in_v7)) {
											now.guard = false;
											goto t6_check;
										}
										r_p8_station_next.size = 0;
										for (i = 0; i < now.p8_station_next.size; ++i) {
											if (i != now.i7) {
												put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
											}
										}
										tmp_INT[0] = v_id;
										fT6_in_v8.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_req;
										fT6_in_v8.field_2 = tmp_INT[1];
										if (cmp_LOCALINT(&fT6_in8, &fT6_in_v8)) {
											now.guard = false;
											goto t6_check;
										}
										r_p5_station_req.size = 0;
										for (i = 0; i < now.p5_station_req.size; ++i) {
											if (i != now.i8) {
												put_msOfLOCALINT(&r_p5_station_req, &now.p5_station_req.content[i]);
											}
										}
										fT6_out1 = v_id;
										if (r_p17_station_idle.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t6_check;
										}
										tmp_INT[0] = v_id;
										fT6_out2.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_next;
										fT6_out2.field_2 = tmp_INT[1];
										tmp_SIGTYPE[2] = v_E;
										fT6_out2.field_3 = tmp_SIGTYPE[2];
										tmp_INT[3] = 0;
										fT6_out2.field_4 = tmp_INT[3];
										fT6_out3 = v_g_ba;
										tmp_INT[0] = v_id;
										fT6_out4.field_1 = tmp_INT[0];
										tmp_INT[1] = 0;
										fT6_out4.field_2 = tmp_INT[1];
										if (r_p10_station_ack.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t6_check;
										}
										tmp_INT[0] = v_id;
										fT6_out5.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_maxmsgs;
										fT6_out5.field_2 = tmp_INT[1];
										if (r_p12_station_maxmsgs.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t6_check;
										}
										tmp_INT[0] = v_id;
										fT6_out6.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_maxmsgs;
										fT6_out6.field_2 = tmp_INT[1];
										if (r_p6_station_msgs.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t6_check;
										}
										tmp_INT[0] = v_id;
										fT6_out7.field_1 = tmp_INT[0];
										tmp_INT[1] = v_station_next;
										fT6_out7.field_2 = tmp_INT[1];
										if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t6_check;
										}
										tmp_INT[0] = v_id;
										fT6_out8.field_1 = tmp_INT[0];
										tmp_INT[1] = 1;
										fT6_out8.field_2 = tmp_INT[1];
										if (r_p5_station_req.size + 1 > MS_MAX_SIZE) {
											now.evaluationFail = true;
											goto t6_check;
										}
t6_check:
										if (now.evaluationFail) now.guard = false;
									} ;
									assert( c_expr { ! now.evaluationFail } );
									if
									:: c_expr { now.guard } ->
										final = 0;
										trans = 6;
										NONDETERM_TRANS
									:: else -> skip
									fi
								fi
							ROF
						ROF
					ROF
				ROF
			ROF
		ROF
		;
		
// transition #7 "t7_ir" enabled binding search
		c_code { now.i1 = now.p17_station_idle.size; } ;
		FOR (i1)
			c_code { now.i3 = now.p10_station_ack.size; } ;
			FOR (i3)
				c_code { now.i4 = now.p7_station_c.size; } ;
				FOR (i4)
					c_code { now.i5 = now.p11_station_maxc.size; } ;
					FOR (i5)
						c_code { now.i6 = now.p12_station_maxmsgs.size; } ;
						FOR (i6)
							c_code { now.i7 = now.p6_station_msgs.size; } ;
							FOR (i7)
								c_code { now.i8 = now.p8_station_next.size; } ;
								FOR (i8)
									c_code { now.i9 = now.p5_station_req.size; } ;
									FOR (i9)
										c_code {
											;
											copy_msOfINT(&now.p17_station_idle, &fT7_in1, now.i1);
											v_id = fT7_in1;
											fT7_in2 = now.p4_env;
											v_I_src = fT7_in2.field_1;
											v_I_type = fT7_in2.field_3;
											v_I_param = fT7_in2.field_4;
											copy_msOfLOCALINT(&now.p10_station_ack, &fT7_in3, now.i3);
											v_station_ack = fT7_in3.field_2;
											copy_msOfLOCALINT(&now.p7_station_c, &fT7_in4, now.i4);
											v_station_c = fT7_in4.field_2;
											copy_msOfLOCALINT(&now.p11_station_maxc, &fT7_in5, now.i5);
											v_station_maxc = fT7_in5.field_2;
											copy_msOfLOCALINT(&now.p12_station_maxmsgs, &fT7_in6, now.i6);
											v_station_maxmsgs = fT7_in6.field_2;
											copy_msOfLOCALINT(&now.p6_station_msgs, &fT7_in7, now.i7);
											v_station_msgs = fT7_in7.field_2;
											copy_msOfLOCALINT(&now.p8_station_next, &fT7_in8, now.i8);
											v_station_next = fT7_in8.field_2;
											copy_msOfLOCALINT(&now.p5_station_req, &fT7_in9, now.i9);
											v_station_req = fT7_in9.field_2;
										} ;
										if
										:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
										:: else ->
											
											c_code {
												now.evaluationFail = now.exprError = false;
												tmp_INT[1] = 0;
												tmp_INT[2] = v_station_msgs;
												tmp_BOOL[0] = ! cmp_INT(&tmp_INT[1], &tmp_INT[2]);
												tmp_SIGTYPE[4] = v_R;
												tmp_SIGTYPE[5] = v_I_type;
												tmp_BOOL[3] = ! cmp_SIGTYPE(&tmp_SIGTYPE[4], &tmp_SIGTYPE[5]);
												now.guard = tmp_BOOL[0] && tmp_BOOL[3];
												if (! now.guard) {
													goto t7_check;
												}
												fT7_in_v1 = v_id;
												if (cmp_INT(&fT7_in1, &fT7_in_v1)) {
													now.guard = false;
													goto t7_check;
												}
												r_p17_station_idle.size = 0;
												for (i = 0; i < now.p17_station_idle.size; ++i) {
													if (i != now.i1) {
														put_msOfINT(&r_p17_station_idle, &now.p17_station_idle.content[i]);
													}
												}
												tmp_INT[0] = v_I_src;
												fT7_in_v2.field_1 = tmp_INT[0];
												tmp_INT[1] = v_id;
												fT7_in_v2.field_2 = tmp_INT[1];
												tmp_SIGTYPE[2] = v_I_type;
												fT7_in_v2.field_3 = tmp_SIGTYPE[2];
												tmp_INT[3] = v_I_param;
												fT7_in_v2.field_4 = tmp_INT[3];
												if (cmp_SIGNAL(&fT7_in2, &fT7_in_v2)) {
													now.guard = false;
													goto t7_check;
												}
												tmp_INT[0] = v_id;
												fT7_in_v3.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_ack;
												fT7_in_v3.field_2 = tmp_INT[1];
												if (cmp_LOCALINT(&fT7_in3, &fT7_in_v3)) {
													now.guard = false;
													goto t7_check;
												}
												r_p10_station_ack.size = 0;
												for (i = 0; i < now.p10_station_ack.size; ++i) {
													if (i != now.i3) {
														put_msOfLOCALINT(&r_p10_station_ack, &now.p10_station_ack.content[i]);
													}
												}
												tmp_INT[0] = v_id;
												fT7_in_v4.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_c;
												fT7_in_v4.field_2 = tmp_INT[1];
												if (cmp_LOCALINT(&fT7_in4, &fT7_in_v4)) {
													now.guard = false;
													goto t7_check;
												}
												r_p7_station_c.size = 0;
												for (i = 0; i < now.p7_station_c.size; ++i) {
													if (i != now.i4) {
														put_msOfLOCALINT(&r_p7_station_c, &now.p7_station_c.content[i]);
													}
												}
												tmp_INT[0] = v_id;
												fT7_in_v5.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_maxc;
												fT7_in_v5.field_2 = tmp_INT[1];
												if (cmp_LOCALINT(&fT7_in5, &fT7_in_v5)) {
													now.guard = false;
													goto t7_check;
												}
												r_p11_station_maxc.size = 0;
												for (i = 0; i < now.p11_station_maxc.size; ++i) {
													if (i != now.i5) {
														put_msOfLOCALINT(&r_p11_station_maxc, &now.p11_station_maxc.content[i]);
													}
												}
												tmp_INT[0] = v_id;
												fT7_in_v6.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_maxmsgs;
												fT7_in_v6.field_2 = tmp_INT[1];
												if (cmp_LOCALINT(&fT7_in6, &fT7_in_v6)) {
													now.guard = false;
													goto t7_check;
												}
												r_p12_station_maxmsgs.size = 0;
												for (i = 0; i < now.p12_station_maxmsgs.size; ++i) {
													if (i != now.i6) {
														put_msOfLOCALINT(&r_p12_station_maxmsgs, &now.p12_station_maxmsgs.content[i]);
													}
												}
												tmp_INT[0] = v_id;
												fT7_in_v7.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_msgs;
												fT7_in_v7.field_2 = tmp_INT[1];
												if (cmp_LOCALINT(&fT7_in7, &fT7_in_v7)) {
													now.guard = false;
													goto t7_check;
												}
												r_p6_station_msgs.size = 0;
												for (i = 0; i < now.p6_station_msgs.size; ++i) {
													if (i != now.i7) {
														put_msOfLOCALINT(&r_p6_station_msgs, &now.p6_station_msgs.content[i]);
													}
												}
												tmp_INT[0] = v_id;
												fT7_in_v8.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_next;
												fT7_in_v8.field_2 = tmp_INT[1];
												if (cmp_LOCALINT(&fT7_in8, &fT7_in_v8)) {
													now.guard = false;
													goto t7_check;
												}
												r_p8_station_next.size = 0;
												for (i = 0; i < now.p8_station_next.size; ++i) {
													if (i != now.i8) {
														put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
													}
												}
												tmp_INT[0] = v_id;
												fT7_in_v9.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_req;
												fT7_in_v9.field_2 = tmp_INT[1];
												if (cmp_LOCALINT(&fT7_in9, &fT7_in_v9)) {
													now.guard = false;
													goto t7_check;
												}
												r_p5_station_req.size = 0;
												for (i = 0; i < now.p5_station_req.size; ++i) {
													if (i != now.i9) {
														put_msOfLOCALINT(&r_p5_station_req, &now.p5_station_req.content[i]);
													}
												}
												fT7_out1 = v_id;
												if (r_p17_station_idle.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t7_check;
												}
												tmp_INT[0] = v_id;
												fT7_out2.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_next;
												fT7_out2.field_2 = tmp_INT[1];
												tmp_SIGTYPE[2] = v_R;
												fT7_out2.field_3 = tmp_SIGTYPE[2];
												tmp_INT[3] = 0;
												fT7_out2.field_4 = tmp_INT[3];
												tmp_INT[0] = v_id;
												fT7_out3.field_1 = tmp_INT[0];
												tmp_INT[1] = 0;
												fT7_out3.field_2 = tmp_INT[1];
												if (r_p10_station_ack.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t7_check;
												}
												tmp_INT[0] = v_id;
												fT7_out4.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_maxc;
												fT7_out4.field_2 = tmp_INT[1];
												if (r_p7_station_c.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t7_check;
												}
												tmp_INT[0] = v_id;
												fT7_out5.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_maxc;
												fT7_out5.field_2 = tmp_INT[1];
												if (r_p11_station_maxc.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t7_check;
												}
												tmp_INT[0] = v_id;
												fT7_out6.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_maxmsgs;
												fT7_out6.field_2 = tmp_INT[1];
												if (r_p12_station_maxmsgs.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t7_check;
												}
												tmp_INT[0] = v_id;
												fT7_out7.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_maxmsgs;
												fT7_out7.field_2 = tmp_INT[1];
												if (r_p6_station_msgs.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t7_check;
												}
												tmp_INT[0] = v_id;
												fT7_out8.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_next;
												fT7_out8.field_2 = tmp_INT[1];
												if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t7_check;
												}
												tmp_INT[0] = v_id;
												fT7_out9.field_1 = tmp_INT[0];
												tmp_INT[1] = 1;
												fT7_out9.field_2 = tmp_INT[1];
												if (r_p5_station_req.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t7_check;
												}
t7_check:
												if (now.evaluationFail) now.guard = false;
											} ;
											assert( c_expr { ! now.evaluationFail } );
											if
											:: c_expr { now.guard } ->
												final = 0;
												trans = 7;
												NONDETERM_TRANS
											:: else -> skip
											fi
										fi
									ROF
								ROF
							ROF
						ROF
					ROF
				ROF
			ROF
		ROF
		;
		
// transition #8 "t8_sd__" enabled binding search
		c_code { now.i1 = now.p3_station_send.size; } ;
		FOR (i1)
			c_code { now.i6 = now.p8_station_next.size; } ;
			FOR (i6)
				c_code {
					;
					copy_msOfINT(&now.p3_station_send, &fT8_in1, now.i1);
					v_id = fT8_in1;
					fT8_in2 = now.p4_env;
					v_I_src = fT8_in2.field_1;
					v_I_type = fT8_in2.field_3;
					v_I_param = fT8_in2.field_4;
					fT8_in3 = now.p14_g_ba;
					v_g_ba = fT8_in3;
					fT8_in4 = now.p16_g_cnt;
					v_g_cnt = fT8_in4;
					fT8_in5 = now.p15_g_dst;
					v_g_dst = fT8_in5;
					copy_msOfLOCALINT(&now.p8_station_next, &fT8_in6, now.i6);
					v_station_next = fT8_in6.field_2;
				} ;
				if
				:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
				:: else ->
					
					c_code {
						now.evaluationFail = now.exprError = false;
						tmp_SIGTYPE[1] = v_D;
						tmp_SIGTYPE[2] = v_I_type;
						tmp_BOOL[0] = ! cmp_SIGTYPE(&tmp_SIGTYPE[1], &tmp_SIGTYPE[2]);
						tmp_INT[4] = v_id;
						tmp_INT[5] = v_g_dst;
						tmp_BOOL[3] = ! cmp_INT(&tmp_INT[4], &tmp_INT[5]);
						now.guard = tmp_BOOL[0] && tmp_BOOL[3];
						if (! now.guard) {
							goto t8_check;
						}
						fT8_in_v1 = v_id;
						if (cmp_INT(&fT8_in1, &fT8_in_v1)) {
							now.guard = false;
							goto t8_check;
						}
						r_p3_station_send.size = 0;
						for (i = 0; i < now.p3_station_send.size; ++i) {
							if (i != now.i1) {
								put_msOfINT(&r_p3_station_send, &now.p3_station_send.content[i]);
							}
						}
						tmp_INT[0] = v_I_src;
						fT8_in_v2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_id;
						fT8_in_v2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_I_type;
						fT8_in_v2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = v_I_param;
						fT8_in_v2.field_4 = tmp_INT[3];
						if (cmp_SIGNAL(&fT8_in2, &fT8_in_v2)) {
							now.guard = false;
							goto t8_check;
						}
						fT8_in_v3 = v_g_ba;
						if (cmp_INT(&fT8_in3, &fT8_in_v3)) {
							now.guard = false;
							goto t8_check;
						}
						fT8_in_v4 = v_g_cnt;
						if (cmp_INT(&fT8_in4, &fT8_in_v4)) {
							now.guard = false;
							goto t8_check;
						}
						fT8_in_v5 = v_g_dst;
						if (cmp_INT(&fT8_in5, &fT8_in_v5)) {
							now.guard = false;
							goto t8_check;
						}
						tmp_INT[0] = v_id;
						fT8_in_v6.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT8_in_v6.field_2 = tmp_INT[1];
						if (cmp_LOCALINT(&fT8_in6, &fT8_in_v6)) {
							now.guard = false;
							goto t8_check;
						}
						r_p8_station_next.size = 0;
						for (i = 0; i < now.p8_station_next.size; ++i) {
							if (i != now.i6) {
								put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
							}
						}
						fT8_out1 = v_id;
						if (r_p3_station_send.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t8_check;
						}
						tmp_INT[0] = v_id;
						fT8_out2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT8_out2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_E;
						fT8_out2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = 0;
						fT8_out2.field_4 = tmp_INT[3];
						fT8_out3 = v_id;
						tmp_INT[0] = v_g_cnt;
						tmp_INT[1] = 1;
						fT8_out4 = tmp_INT[0] - tmp_INT[1];
						fT8_out5 = 0;
						tmp_INT[0] = v_id;
						fT8_out6.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT8_out6.field_2 = tmp_INT[1];
						if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t8_check;
						}
t8_check:
						if (now.evaluationFail) now.guard = false;
					} ;
					assert( c_expr { ! now.evaluationFail } );
					if
					:: c_expr { now.guard } ->
						final = 0;
						trans = 8;
						NONDETERM_TRANS
					:: else -> skip
					fi
				fi
			ROF
		ROF
		;
		
// transition #9 "t9_sdn" enabled binding search
		c_code { now.i1 = now.p3_station_send.size; } ;
		FOR (i1)
			c_code { now.i5 = now.p8_station_next.size; } ;
			FOR (i5)
				c_code {
					;
					copy_msOfINT(&now.p3_station_send, &fT9_in1, now.i1);
					v_id = fT9_in1;
					fT9_in2 = now.p4_env;
					v_I_src = fT9_in2.field_1;
					v_I_type = fT9_in2.field_3;
					v_I_param = fT9_in2.field_4;
					fT9_in3 = now.p14_g_ba;
					v_g_ba = fT9_in3;
					fT9_in4 = now.p15_g_dst;
					v_g_dst = fT9_in4;
					copy_msOfLOCALINT(&now.p8_station_next, &fT9_in5, now.i5);
					v_station_next = fT9_in5.field_2;
				} ;
				if
				:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
				:: else ->
					
					c_code {
						now.evaluationFail = now.exprError = false;
						tmp_SIGTYPE[1] = v_D;
						tmp_SIGTYPE[2] = v_I_type;
						tmp_BOOL[0] = ! cmp_SIGTYPE(&tmp_SIGTYPE[1], &tmp_SIGTYPE[2]);
						tmp_INT[4] = v_id;
						tmp_INT[5] = v_g_dst;
						tmp_BOOL[3] = cmp_INT(&tmp_INT[4], &tmp_INT[5]);
						now.guard = tmp_BOOL[0] && tmp_BOOL[3];
						if (! now.guard) {
							goto t9_check;
						}
						fT9_in_v1 = v_id;
						if (cmp_INT(&fT9_in1, &fT9_in_v1)) {
							now.guard = false;
							goto t9_check;
						}
						r_p3_station_send.size = 0;
						for (i = 0; i < now.p3_station_send.size; ++i) {
							if (i != now.i1) {
								put_msOfINT(&r_p3_station_send, &now.p3_station_send.content[i]);
							}
						}
						tmp_INT[0] = v_I_src;
						fT9_in_v2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_id;
						fT9_in_v2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_I_type;
						fT9_in_v2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = v_I_param;
						fT9_in_v2.field_4 = tmp_INT[3];
						if (cmp_SIGNAL(&fT9_in2, &fT9_in_v2)) {
							now.guard = false;
							goto t9_check;
						}
						fT9_in_v3 = v_g_ba;
						if (cmp_INT(&fT9_in3, &fT9_in_v3)) {
							now.guard = false;
							goto t9_check;
						}
						fT9_in_v4 = v_g_dst;
						if (cmp_INT(&fT9_in4, &fT9_in_v4)) {
							now.guard = false;
							goto t9_check;
						}
						tmp_INT[0] = v_id;
						fT9_in_v5.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT9_in_v5.field_2 = tmp_INT[1];
						if (cmp_LOCALINT(&fT9_in5, &fT9_in_v5)) {
							now.guard = false;
							goto t9_check;
						}
						r_p8_station_next.size = 0;
						for (i = 0; i < now.p8_station_next.size; ++i) {
							if (i != now.i5) {
								put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
							}
						}
						fT9_out1 = v_id;
						if (r_p3_station_send.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t9_check;
						}
						tmp_INT[0] = v_id;
						fT9_out2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT9_out2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_D;
						fT9_out2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = 0;
						fT9_out2.field_4 = tmp_INT[3];
						fT9_out3 = v_id;
						fT9_out4 = v_g_dst;
						tmp_INT[0] = v_id;
						fT9_out5.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT9_out5.field_2 = tmp_INT[1];
						if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t9_check;
						}
t9_check:
						if (now.evaluationFail) now.guard = false;
					} ;
					assert( c_expr { ! now.evaluationFail } );
					if
					:: c_expr { now.guard } ->
						final = 0;
						trans = 9;
						NONDETERM_TRANS
					:: else -> skip
					fi
				fi
			ROF
		ROF
		;
		
// transition #10 "t10_se" enabled binding search
		c_code { now.i1 = now.p3_station_send.size; } ;
		FOR (i1)
			c_code { now.i3 = now.p13_NonDeterm0.size; } ;
			FOR (i3)
				c_code { now.i7 = now.p10_station_ack.size; } ;
				FOR (i7)
					c_code { now.i8 = now.p7_station_c.size; } ;
					FOR (i8)
						c_code { now.i9 = now.p6_station_msgs.size; } ;
						FOR (i9)
							c_code { now.i10 = now.p8_station_next.size; } ;
							FOR (i10)
								c_code { now.i11 = now.p9_station_other.size; } ;
								FOR (i11)
									c_code { now.i12 = now.p5_station_req.size; } ;
									FOR (i12)
										c_code {
											;
											copy_msOfINT(&now.p3_station_send, &fT10_in1, now.i1);
											v_id = fT10_in1;
											fT10_in2 = now.p4_env;
											v_I_src = fT10_in2.field_1;
											v_I_type = fT10_in2.field_3;
											v_I_param = fT10_in2.field_4;
											copy_msOfINT(&now.p13_NonDeterm0, &fT10_in3, now.i3);
											v_NonDeterm0 = fT10_in3;
											fT10_in4 = now.p14_g_ba;
											v_g_ba = fT10_in4;
											fT10_in5 = now.p16_g_cnt;
											v_g_cnt = fT10_in5;
											fT10_in6 = now.p15_g_dst;
											v_g_dst = fT10_in6;
											copy_msOfLOCALINT(&now.p10_station_ack, &fT10_in7, now.i7);
											v_station_ack = fT10_in7.field_2;
											copy_msOfLOCALINT(&now.p7_station_c, &fT10_in8, now.i8);
											v_station_c = fT10_in8.field_2;
											copy_msOfLOCALINT(&now.p6_station_msgs, &fT10_in9, now.i9);
											v_station_msgs = fT10_in9.field_2;
											copy_msOfLOCALINT(&now.p8_station_next, &fT10_in10, now.i10);
											v_station_next = fT10_in10.field_2;
											copy_msOfLOCALARRAYINT(&now.p9_station_other, &fT10_in11, now.i11);
											v_station_other = fT10_in11.field_2;
											copy_msOfLOCALINT(&now.p5_station_req, &fT10_in12, now.i12);
											v_station_req = fT10_in12.field_2;
										} ;
										if
										:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
										:: else ->
											
											c_code {
												now.evaluationFail = now.exprError = false;
												tmp_SIGTYPE[0] = v_E;
												tmp_SIGTYPE[1] = v_I_type;
												now.guard = ! cmp_SIGTYPE(&tmp_SIGTYPE[0], &tmp_SIGTYPE[1]);
												if (! now.guard) {
													goto t10_check;
												}
												fT10_in_v1 = v_id;
												if (cmp_INT(&fT10_in1, &fT10_in_v1)) {
													now.guard = false;
													goto t10_check;
												}
												tmp_INT[0] = v_I_src;
												fT10_in_v2.field_1 = tmp_INT[0];
												tmp_INT[1] = v_id;
												fT10_in_v2.field_2 = tmp_INT[1];
												tmp_SIGTYPE[2] = v_I_type;
												fT10_in_v2.field_3 = tmp_SIGTYPE[2];
												tmp_INT[3] = v_I_param;
												fT10_in_v2.field_4 = tmp_INT[3];
												if (cmp_SIGNAL(&fT10_in2, &fT10_in_v2)) {
													now.guard = false;
													goto t10_check;
												}
												fT10_in_v3 = v_NonDeterm0;
												if (cmp_INT(&fT10_in3, &fT10_in_v3)) {
													now.guard = false;
													goto t10_check;
												}
												r_p13_NonDeterm0.size = 0;
												for (i = 0; i < now.p13_NonDeterm0.size; ++i) {
													if (i != now.i3) {
														put_msOfINT(&r_p13_NonDeterm0, &now.p13_NonDeterm0.content[i]);
													}
												}
												fT10_in_v4 = v_g_ba;
												if (cmp_INT(&fT10_in4, &fT10_in_v4)) {
													now.guard = false;
													goto t10_check;
												}
												fT10_in_v5 = v_g_cnt;
												if (cmp_INT(&fT10_in5, &fT10_in_v5)) {
													now.guard = false;
													goto t10_check;
												}
												fT10_in_v6 = v_g_dst;
												if (cmp_INT(&fT10_in6, &fT10_in_v6)) {
													now.guard = false;
													goto t10_check;
												}
												tmp_INT[0] = v_id;
												fT10_in_v7.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_ack;
												fT10_in_v7.field_2 = tmp_INT[1];
												if (cmp_LOCALINT(&fT10_in7, &fT10_in_v7)) {
													now.guard = false;
													goto t10_check;
												}
												r_p10_station_ack.size = 0;
												for (i = 0; i < now.p10_station_ack.size; ++i) {
													if (i != now.i7) {
														put_msOfLOCALINT(&r_p10_station_ack, &now.p10_station_ack.content[i]);
													}
												}
												tmp_INT[0] = v_id;
												fT10_in_v8.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_c;
												fT10_in_v8.field_2 = tmp_INT[1];
												if (cmp_LOCALINT(&fT10_in8, &fT10_in_v8)) {
													now.guard = false;
													goto t10_check;
												}
												r_p7_station_c.size = 0;
												for (i = 0; i < now.p7_station_c.size; ++i) {
													if (i != now.i8) {
														put_msOfLOCALINT(&r_p7_station_c, &now.p7_station_c.content[i]);
													}
												}
												tmp_INT[0] = v_id;
												fT10_in_v9.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_msgs;
												fT10_in_v9.field_2 = tmp_INT[1];
												if (cmp_LOCALINT(&fT10_in9, &fT10_in_v9)) {
													now.guard = false;
													goto t10_check;
												}
												r_p6_station_msgs.size = 0;
												for (i = 0; i < now.p6_station_msgs.size; ++i) {
													if (i != now.i9) {
														put_msOfLOCALINT(&r_p6_station_msgs, &now.p6_station_msgs.content[i]);
													}
												}
												tmp_INT[0] = v_id;
												fT10_in_v10.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_next;
												fT10_in_v10.field_2 = tmp_INT[1];
												if (cmp_LOCALINT(&fT10_in10, &fT10_in_v10)) {
													now.guard = false;
													goto t10_check;
												}
												r_p8_station_next.size = 0;
												for (i = 0; i < now.p8_station_next.size; ++i) {
													if (i != now.i10) {
														put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
													}
												}
												tmp_INT[0] = v_id;
												fT10_in_v11.field_1 = tmp_INT[0];
												tmp_ARRAYINT[1] = v_station_other;
												fT10_in_v11.field_2 = tmp_ARRAYINT[1];
												if (cmp_LOCALARRAYINT(&fT10_in11, &fT10_in_v11)) {
													now.guard = false;
													goto t10_check;
												}
												r_p9_station_other.size = 0;
												for (i = 0; i < now.p9_station_other.size; ++i) {
													if (i != now.i11) {
														put_msOfLOCALARRAYINT(&r_p9_station_other, &now.p9_station_other.content[i]);
													}
												}
												tmp_INT[0] = v_id;
												fT10_in_v12.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_req;
												fT10_in_v12.field_2 = tmp_INT[1];
												if (cmp_LOCALINT(&fT10_in12, &fT10_in_v12)) {
													now.guard = false;
													goto t10_check;
												}
												r_p5_station_req.size = 0;
												for (i = 0; i < now.p5_station_req.size; ++i) {
													if (i != now.i12) {
														put_msOfLOCALINT(&r_p5_station_req, &now.p5_station_req.content[i]);
													}
												}
												fT10_out1 = v_id;
												if (now.p17_station_idle.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t10_check;
												}
												tmp_INT[0] = v_id;
												fT10_out2.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_next;
												fT10_out2.field_2 = tmp_INT[1];
												tmp_SIGTYPE[2] = v_D;
												fT10_out2.field_3 = tmp_SIGTYPE[2];
												tmp_INT[3] = 0;
												fT10_out2.field_4 = tmp_INT[3];
												fT10_out3 = v_NonDeterm0;
												if (r_p13_NonDeterm0.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t10_check;
												}
												fT10_out4 = v_id;
												tmp_INT[0] = v_g_cnt;
												tmp_INT[1] = 1;
												fT10_out5 = tmp_INT[0] + tmp_INT[1];
												tmp_ARRAYINT[0] = v_station_other;
												tmp_INT[3] = v_NonDeterm0;
												tmp_INT[4] = 0;
												tmp_BOOL[2] = ! cmp_INT(&tmp_INT[3], &tmp_INT[4]);
												if (tmp_BOOL[2]) {
													tmp_INT[5] = 0;
													tmp_INT[1] = tmp_INT[5];
												}
												else {
													tmp_INT[6] = 1;
													tmp_INT[1] = tmp_INT[6];
												}
												if (fails_nth_ARRAYINT(&now.evaluationFail, &now.exprError, &tmp_ARRAYINT[0], tmp_INT[1])) goto t10_check;
												nth_ARRAYINT(&fT10_out6, &tmp_ARRAYINT[0], tmp_INT[1]);
												tmp_INT[0] = v_id;
												fT10_out7.field_1 = tmp_INT[0];
												tmp_INT[1] = 1;
												fT10_out7.field_2 = tmp_INT[1];
												if (r_p10_station_ack.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t10_check;
												}
												tmp_INT[0] = v_id;
												fT10_out8.field_1 = tmp_INT[0];
												tmp_INT[2] = v_station_c;
												tmp_INT[3] = 1;
												tmp_INT[1] = tmp_INT[2] - tmp_INT[3];
												fT10_out8.field_2 = tmp_INT[1];
												if (r_p7_station_c.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t10_check;
												}
												tmp_INT[0] = v_id;
												fT10_out9.field_1 = tmp_INT[0];
												tmp_INT[2] = v_station_msgs;
												tmp_INT[3] = 1;
												tmp_INT[1] = tmp_INT[2] - tmp_INT[3];
												fT10_out9.field_2 = tmp_INT[1];
												if (r_p6_station_msgs.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t10_check;
												}
												tmp_INT[0] = v_id;
												fT10_out10.field_1 = tmp_INT[0];
												tmp_INT[1] = v_station_next;
												fT10_out10.field_2 = tmp_INT[1];
												if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t10_check;
												}
												tmp_INT[0] = v_id;
												fT10_out11.field_1 = tmp_INT[0];
												tmp_ARRAYINT[1] = v_station_other;
												fT10_out11.field_2 = tmp_ARRAYINT[1];
												if (r_p9_station_other.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t10_check;
												}
												tmp_INT[0] = v_id;
												fT10_out12.field_1 = tmp_INT[0];
												tmp_INT[1] = 0;
												fT10_out12.field_2 = tmp_INT[1];
												if (r_p5_station_req.size + 1 > MS_MAX_SIZE) {
													now.evaluationFail = true;
													goto t10_check;
												}
t10_check:
												if (now.evaluationFail) now.guard = false;
											} ;
											assert( c_expr { ! now.evaluationFail } );
											assert( c_expr { ! now.exprError } );
											if
											:: c_expr { now.guard } ->
												final = 0;
												trans = 10;
												NONDETERM_TRANS
											:: else -> skip
											fi
										fi
									ROF
								ROF
							ROF
						ROF
					ROF
				ROF
			ROF
		ROF
		;
		
// transition #11 "t11_sr" enabled binding search
		c_code { now.i1 = now.p3_station_send.size; } ;
		FOR (i1)
			c_code { now.i3 = now.p7_station_c.size; } ;
			FOR (i3)
				c_code { now.i4 = now.p11_station_maxc.size; } ;
				FOR (i4)
					c_code { now.i5 = now.p8_station_next.size; } ;
					FOR (i5)
						c_code {
							;
							copy_msOfINT(&now.p3_station_send, &fT11_in1, now.i1);
							v_id = fT11_in1;
							fT11_in2 = now.p4_env;
							v_I_src = fT11_in2.field_1;
							v_I_type = fT11_in2.field_3;
							v_I_param = fT11_in2.field_4;
							copy_msOfLOCALINT(&now.p7_station_c, &fT11_in3, now.i3);
							v_station_c = fT11_in3.field_2;
							copy_msOfLOCALINT(&now.p11_station_maxc, &fT11_in4, now.i4);
							v_station_maxc = fT11_in4.field_2;
							copy_msOfLOCALINT(&now.p8_station_next, &fT11_in5, now.i5);
							v_station_next = fT11_in5.field_2;
						} ;
						if
						:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
						:: else ->
							
							c_code {
								now.evaluationFail = now.exprError = false;
								tmp_SIGTYPE[0] = v_R;
								tmp_SIGTYPE[1] = v_I_type;
								now.guard = ! cmp_SIGTYPE(&tmp_SIGTYPE[0], &tmp_SIGTYPE[1]);
								if (! now.guard) {
									goto t11_check;
								}
								fT11_in_v1 = v_id;
								if (cmp_INT(&fT11_in1, &fT11_in_v1)) {
									now.guard = false;
									goto t11_check;
								}
								tmp_INT[0] = v_I_src;
								fT11_in_v2.field_1 = tmp_INT[0];
								tmp_INT[1] = v_id;
								fT11_in_v2.field_2 = tmp_INT[1];
								tmp_SIGTYPE[2] = v_I_type;
								fT11_in_v2.field_3 = tmp_SIGTYPE[2];
								tmp_INT[3] = v_I_param;
								fT11_in_v2.field_4 = tmp_INT[3];
								if (cmp_SIGNAL(&fT11_in2, &fT11_in_v2)) {
									now.guard = false;
									goto t11_check;
								}
								tmp_INT[0] = v_id;
								fT11_in_v3.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_c;
								fT11_in_v3.field_2 = tmp_INT[1];
								if (cmp_LOCALINT(&fT11_in3, &fT11_in_v3)) {
									now.guard = false;
									goto t11_check;
								}
								r_p7_station_c.size = 0;
								for (i = 0; i < now.p7_station_c.size; ++i) {
									if (i != now.i3) {
										put_msOfLOCALINT(&r_p7_station_c, &now.p7_station_c.content[i]);
									}
								}
								tmp_INT[0] = v_id;
								fT11_in_v4.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_maxc;
								fT11_in_v4.field_2 = tmp_INT[1];
								if (cmp_LOCALINT(&fT11_in4, &fT11_in_v4)) {
									now.guard = false;
									goto t11_check;
								}
								r_p11_station_maxc.size = 0;
								for (i = 0; i < now.p11_station_maxc.size; ++i) {
									if (i != now.i4) {
										put_msOfLOCALINT(&r_p11_station_maxc, &now.p11_station_maxc.content[i]);
									}
								}
								tmp_INT[0] = v_id;
								fT11_in_v5.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_next;
								fT11_in_v5.field_2 = tmp_INT[1];
								if (cmp_LOCALINT(&fT11_in5, &fT11_in_v5)) {
									now.guard = false;
									goto t11_check;
								}
								r_p8_station_next.size = 0;
								for (i = 0; i < now.p8_station_next.size; ++i) {
									if (i != now.i5) {
										put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
									}
								}
								fT11_out1 = v_id;
								if (now.p17_station_idle.size + 1 > MS_MAX_SIZE) {
									now.evaluationFail = true;
									goto t11_check;
								}
								tmp_INT[0] = v_id;
								fT11_out2.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_next;
								fT11_out2.field_2 = tmp_INT[1];
								tmp_SIGTYPE[2] = v_R;
								fT11_out2.field_3 = tmp_SIGTYPE[2];
								tmp_INT[3] = 0;
								fT11_out2.field_4 = tmp_INT[3];
								tmp_INT[0] = v_id;
								fT11_out3.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_maxc;
								fT11_out3.field_2 = tmp_INT[1];
								if (r_p7_station_c.size + 1 > MS_MAX_SIZE) {
									now.evaluationFail = true;
									goto t11_check;
								}
								tmp_INT[0] = v_id;
								fT11_out4.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_maxc;
								fT11_out4.field_2 = tmp_INT[1];
								if (r_p11_station_maxc.size + 1 > MS_MAX_SIZE) {
									now.evaluationFail = true;
									goto t11_check;
								}
								tmp_INT[0] = v_id;
								fT11_out5.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_next;
								fT11_out5.field_2 = tmp_INT[1];
								if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
									now.evaluationFail = true;
									goto t11_check;
								}
t11_check:
								if (now.evaluationFail) now.guard = false;
							} ;
							assert( c_expr { ! now.evaluationFail } );
							if
							:: c_expr { now.guard } ->
								final = 0;
								trans = 11;
								NONDETERM_TRANS
							:: else -> skip
							fi
						fi
					ROF
				ROF
			ROF
		ROF
		;
		
// transition #12 "t12_rd__" enabled binding search
		c_code { now.i1 = now.p1_station_reset.size; } ;
		FOR (i1)
			c_code { now.i5 = now.p8_station_next.size; } ;
			FOR (i5)
				c_code {
					;
					copy_msOfINT(&now.p1_station_reset, &fT12_in1, now.i1);
					v_id = fT12_in1;
					fT12_in2 = now.p4_env;
					v_I_src = fT12_in2.field_1;
					v_I_type = fT12_in2.field_3;
					v_I_param = fT12_in2.field_4;
					fT12_in3 = now.p16_g_cnt;
					v_g_cnt = fT12_in3;
					fT12_in4 = now.p15_g_dst;
					v_g_dst = fT12_in4;
					copy_msOfLOCALINT(&now.p8_station_next, &fT12_in5, now.i5);
					v_station_next = fT12_in5.field_2;
				} ;
				if
				:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
				:: else ->
					
					c_code {
						now.evaluationFail = now.exprError = false;
						tmp_SIGTYPE[1] = v_D;
						tmp_SIGTYPE[2] = v_I_type;
						tmp_BOOL[0] = ! cmp_SIGTYPE(&tmp_SIGTYPE[1], &tmp_SIGTYPE[2]);
						tmp_INT[4] = v_id;
						tmp_INT[5] = v_g_dst;
						tmp_BOOL[3] = ! cmp_INT(&tmp_INT[4], &tmp_INT[5]);
						now.guard = tmp_BOOL[0] && tmp_BOOL[3];
						if (! now.guard) {
							goto t12_check;
						}
						fT12_in_v1 = v_id;
						if (cmp_INT(&fT12_in1, &fT12_in_v1)) {
							now.guard = false;
							goto t12_check;
						}
						r_p1_station_reset.size = 0;
						for (i = 0; i < now.p1_station_reset.size; ++i) {
							if (i != now.i1) {
								put_msOfINT(&r_p1_station_reset, &now.p1_station_reset.content[i]);
							}
						}
						tmp_INT[0] = v_I_src;
						fT12_in_v2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_id;
						fT12_in_v2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_I_type;
						fT12_in_v2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = v_I_param;
						fT12_in_v2.field_4 = tmp_INT[3];
						if (cmp_SIGNAL(&fT12_in2, &fT12_in_v2)) {
							now.guard = false;
							goto t12_check;
						}
						fT12_in_v3 = v_g_cnt;
						if (cmp_INT(&fT12_in3, &fT12_in_v3)) {
							now.guard = false;
							goto t12_check;
						}
						fT12_in_v4 = v_g_dst;
						if (cmp_INT(&fT12_in4, &fT12_in_v4)) {
							now.guard = false;
							goto t12_check;
						}
						tmp_INT[0] = v_id;
						fT12_in_v5.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT12_in_v5.field_2 = tmp_INT[1];
						if (cmp_LOCALINT(&fT12_in5, &fT12_in_v5)) {
							now.guard = false;
							goto t12_check;
						}
						r_p8_station_next.size = 0;
						for (i = 0; i < now.p8_station_next.size; ++i) {
							if (i != now.i5) {
								put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
							}
						}
						fT12_out1 = v_id;
						if (r_p1_station_reset.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t12_check;
						}
						tmp_INT[0] = v_id;
						fT12_out2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT12_out2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_E;
						fT12_out2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = 0;
						fT12_out2.field_4 = tmp_INT[3];
						tmp_INT[0] = v_g_cnt;
						tmp_INT[1] = 1;
						fT12_out3 = tmp_INT[0] - tmp_INT[1];
						fT12_out4 = 0;
						tmp_INT[0] = v_id;
						fT12_out5.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT12_out5.field_2 = tmp_INT[1];
						if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t12_check;
						}
t12_check:
						if (now.evaluationFail) now.guard = false;
					} ;
					assert( c_expr { ! now.evaluationFail } );
					if
					:: c_expr { now.guard } ->
						final = 0;
						trans = 12;
						NONDETERM_TRANS
					:: else -> skip
					fi
				fi
			ROF
		ROF
		;
		
// transition #13 "t13_rdn" enabled binding search
		c_code { now.i1 = now.p1_station_reset.size; } ;
		FOR (i1)
			c_code { now.i4 = now.p8_station_next.size; } ;
			FOR (i4)
				c_code {
					;
					copy_msOfINT(&now.p1_station_reset, &fT13_in1, now.i1);
					v_id = fT13_in1;
					fT13_in2 = now.p4_env;
					v_I_src = fT13_in2.field_1;
					v_I_type = fT13_in2.field_3;
					v_I_param = fT13_in2.field_4;
					fT13_in3 = now.p15_g_dst;
					v_g_dst = fT13_in3;
					copy_msOfLOCALINT(&now.p8_station_next, &fT13_in4, now.i4);
					v_station_next = fT13_in4.field_2;
				} ;
				if
				:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
				:: else ->
					
					c_code {
						now.evaluationFail = now.exprError = false;
						tmp_SIGTYPE[1] = v_D;
						tmp_SIGTYPE[2] = v_I_type;
						tmp_BOOL[0] = ! cmp_SIGTYPE(&tmp_SIGTYPE[1], &tmp_SIGTYPE[2]);
						tmp_INT[4] = v_id;
						tmp_INT[5] = v_g_dst;
						tmp_BOOL[3] = cmp_INT(&tmp_INT[4], &tmp_INT[5]);
						now.guard = tmp_BOOL[0] && tmp_BOOL[3];
						if (! now.guard) {
							goto t13_check;
						}
						fT13_in_v1 = v_id;
						if (cmp_INT(&fT13_in1, &fT13_in_v1)) {
							now.guard = false;
							goto t13_check;
						}
						r_p1_station_reset.size = 0;
						for (i = 0; i < now.p1_station_reset.size; ++i) {
							if (i != now.i1) {
								put_msOfINT(&r_p1_station_reset, &now.p1_station_reset.content[i]);
							}
						}
						tmp_INT[0] = v_I_src;
						fT13_in_v2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_id;
						fT13_in_v2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_I_type;
						fT13_in_v2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = v_I_param;
						fT13_in_v2.field_4 = tmp_INT[3];
						if (cmp_SIGNAL(&fT13_in2, &fT13_in_v2)) {
							now.guard = false;
							goto t13_check;
						}
						fT13_in_v3 = v_g_dst;
						if (cmp_INT(&fT13_in3, &fT13_in_v3)) {
							now.guard = false;
							goto t13_check;
						}
						tmp_INT[0] = v_id;
						fT13_in_v4.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT13_in_v4.field_2 = tmp_INT[1];
						if (cmp_LOCALINT(&fT13_in4, &fT13_in_v4)) {
							now.guard = false;
							goto t13_check;
						}
						r_p8_station_next.size = 0;
						for (i = 0; i < now.p8_station_next.size; ++i) {
							if (i != now.i4) {
								put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
							}
						}
						fT13_out1 = v_id;
						if (r_p1_station_reset.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t13_check;
						}
						tmp_INT[0] = v_id;
						fT13_out2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT13_out2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_D;
						fT13_out2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = 0;
						fT13_out2.field_4 = tmp_INT[3];
						fT13_out3 = v_g_dst;
						tmp_INT[0] = v_id;
						fT13_out4.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT13_out4.field_2 = tmp_INT[1];
						if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t13_check;
						}
t13_check:
						if (now.evaluationFail) now.guard = false;
					} ;
					assert( c_expr { ! now.evaluationFail } );
					if
					:: c_expr { now.guard } ->
						final = 0;
						trans = 13;
						NONDETERM_TRANS
					:: else -> skip
					fi
				fi
			ROF
		ROF
		;
		
// transition #14 "t14_re" enabled binding search
		c_code { now.i1 = now.p1_station_reset.size; } ;
		FOR (i1)
			c_code { now.i3 = now.p8_station_next.size; } ;
			FOR (i3)
				c_code {
					;
					copy_msOfINT(&now.p1_station_reset, &fT14_in1, now.i1);
					v_id = fT14_in1;
					fT14_in2 = now.p4_env;
					v_I_src = fT14_in2.field_1;
					v_I_type = fT14_in2.field_3;
					v_I_param = fT14_in2.field_4;
					copy_msOfLOCALINT(&now.p8_station_next, &fT14_in3, now.i3);
					v_station_next = fT14_in3.field_2;
				} ;
				if
				:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
				:: else ->
					
					c_code {
						now.evaluationFail = now.exprError = false;
						tmp_SIGTYPE[0] = v_E;
						tmp_SIGTYPE[1] = v_I_type;
						now.guard = ! cmp_SIGTYPE(&tmp_SIGTYPE[0], &tmp_SIGTYPE[1]);
						if (! now.guard) {
							goto t14_check;
						}
						fT14_in_v1 = v_id;
						if (cmp_INT(&fT14_in1, &fT14_in_v1)) {
							now.guard = false;
							goto t14_check;
						}
						r_p1_station_reset.size = 0;
						for (i = 0; i < now.p1_station_reset.size; ++i) {
							if (i != now.i1) {
								put_msOfINT(&r_p1_station_reset, &now.p1_station_reset.content[i]);
							}
						}
						tmp_INT[0] = v_I_src;
						fT14_in_v2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_id;
						fT14_in_v2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_I_type;
						fT14_in_v2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = v_I_param;
						fT14_in_v2.field_4 = tmp_INT[3];
						if (cmp_SIGNAL(&fT14_in2, &fT14_in_v2)) {
							now.guard = false;
							goto t14_check;
						}
						tmp_INT[0] = v_id;
						fT14_in_v3.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT14_in_v3.field_2 = tmp_INT[1];
						if (cmp_LOCALINT(&fT14_in3, &fT14_in_v3)) {
							now.guard = false;
							goto t14_check;
						}
						r_p8_station_next.size = 0;
						for (i = 0; i < now.p8_station_next.size; ++i) {
							if (i != now.i3) {
								put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
							}
						}
						fT14_out1 = v_id;
						if (r_p1_station_reset.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t14_check;
						}
						tmp_INT[0] = v_id;
						fT14_out2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT14_out2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_E;
						fT14_out2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = 0;
						fT14_out2.field_4 = tmp_INT[3];
						tmp_INT[0] = v_id;
						fT14_out3.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT14_out3.field_2 = tmp_INT[1];
						if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t14_check;
						}
t14_check:
						if (now.evaluationFail) now.guard = false;
					} ;
					assert( c_expr { ! now.evaluationFail } );
					if
					:: c_expr { now.guard } ->
						final = 0;
						trans = 14;
						NONDETERM_TRANS
					:: else -> skip
					fi
				fi
			ROF
		ROF
		;
		
// transition #15 "t15_rr" enabled binding search
		c_code { now.i1 = now.p1_station_reset.size; } ;
		FOR (i1)
			c_code { now.i4 = now.p7_station_c.size; } ;
			FOR (i4)
				c_code { now.i5 = now.p11_station_maxc.size; } ;
				FOR (i5)
					c_code { now.i6 = now.p8_station_next.size; } ;
					FOR (i6)
						c_code {
							;
							copy_msOfINT(&now.p1_station_reset, &fT15_in1, now.i1);
							v_id = fT15_in1;
							fT15_in2 = now.p4_env;
							v_I_src = fT15_in2.field_1;
							v_I_type = fT15_in2.field_3;
							v_I_param = fT15_in2.field_4;
							fT15_in3 = now.p14_g_ba;
							v_g_ba = fT15_in3;
							copy_msOfLOCALINT(&now.p7_station_c, &fT15_in4, now.i4);
							v_station_c = fT15_in4.field_2;
							copy_msOfLOCALINT(&now.p11_station_maxc, &fT15_in5, now.i5);
							v_station_maxc = fT15_in5.field_2;
							copy_msOfLOCALINT(&now.p8_station_next, &fT15_in6, now.i6);
							v_station_next = fT15_in6.field_2;
						} ;
						if
						:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
						:: else ->
							
							c_code {
								now.evaluationFail = now.exprError = false;
								tmp_SIGTYPE[0] = v_R;
								tmp_SIGTYPE[1] = v_I_type;
								now.guard = ! cmp_SIGTYPE(&tmp_SIGTYPE[0], &tmp_SIGTYPE[1]);
								if (! now.guard) {
									goto t15_check;
								}
								fT15_in_v1 = v_id;
								if (cmp_INT(&fT15_in1, &fT15_in_v1)) {
									now.guard = false;
									goto t15_check;
								}
								tmp_INT[0] = v_I_src;
								fT15_in_v2.field_1 = tmp_INT[0];
								tmp_INT[1] = v_id;
								fT15_in_v2.field_2 = tmp_INT[1];
								tmp_SIGTYPE[2] = v_I_type;
								fT15_in_v2.field_3 = tmp_SIGTYPE[2];
								tmp_INT[3] = v_I_param;
								fT15_in_v2.field_4 = tmp_INT[3];
								if (cmp_SIGNAL(&fT15_in2, &fT15_in_v2)) {
									now.guard = false;
									goto t15_check;
								}
								fT15_in_v3 = v_g_ba;
								if (cmp_INT(&fT15_in3, &fT15_in_v3)) {
									now.guard = false;
									goto t15_check;
								}
								tmp_INT[0] = v_id;
								fT15_in_v4.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_c;
								fT15_in_v4.field_2 = tmp_INT[1];
								if (cmp_LOCALINT(&fT15_in4, &fT15_in_v4)) {
									now.guard = false;
									goto t15_check;
								}
								r_p7_station_c.size = 0;
								for (i = 0; i < now.p7_station_c.size; ++i) {
									if (i != now.i4) {
										put_msOfLOCALINT(&r_p7_station_c, &now.p7_station_c.content[i]);
									}
								}
								tmp_INT[0] = v_id;
								fT15_in_v5.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_maxc;
								fT15_in_v5.field_2 = tmp_INT[1];
								if (cmp_LOCALINT(&fT15_in5, &fT15_in_v5)) {
									now.guard = false;
									goto t15_check;
								}
								r_p11_station_maxc.size = 0;
								for (i = 0; i < now.p11_station_maxc.size; ++i) {
									if (i != now.i5) {
										put_msOfLOCALINT(&r_p11_station_maxc, &now.p11_station_maxc.content[i]);
									}
								}
								tmp_INT[0] = v_id;
								fT15_in_v6.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_next;
								fT15_in_v6.field_2 = tmp_INT[1];
								if (cmp_LOCALINT(&fT15_in6, &fT15_in_v6)) {
									now.guard = false;
									goto t15_check;
								}
								r_p8_station_next.size = 0;
								for (i = 0; i < now.p8_station_next.size; ++i) {
									if (i != now.i6) {
										put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
									}
								}
								fT15_out1 = v_id;
								if (now.p17_station_idle.size + 1 > MS_MAX_SIZE) {
									now.evaluationFail = true;
									goto t15_check;
								}
								tmp_INT[0] = v_id;
								fT15_out2.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_next;
								fT15_out2.field_2 = tmp_INT[1];
								tmp_SIGTYPE[2] = v_E;
								fT15_out2.field_3 = tmp_SIGTYPE[2];
								tmp_INT[3] = 0;
								fT15_out2.field_4 = tmp_INT[3];
								fT15_out3 = v_id;
								tmp_INT[0] = v_id;
								fT15_out4.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_maxc;
								fT15_out4.field_2 = tmp_INT[1];
								if (r_p7_station_c.size + 1 > MS_MAX_SIZE) {
									now.evaluationFail = true;
									goto t15_check;
								}
								tmp_INT[0] = v_id;
								fT15_out5.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_maxc;
								fT15_out5.field_2 = tmp_INT[1];
								if (r_p11_station_maxc.size + 1 > MS_MAX_SIZE) {
									now.evaluationFail = true;
									goto t15_check;
								}
								tmp_INT[0] = v_id;
								fT15_out6.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_next;
								fT15_out6.field_2 = tmp_INT[1];
								if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
									now.evaluationFail = true;
									goto t15_check;
								}
t15_check:
								if (now.evaluationFail) now.guard = false;
							} ;
							assert( c_expr { ! now.evaluationFail } );
							if
							:: c_expr { now.guard } ->
								final = 0;
								trans = 15;
								NONDETERM_TRANS
							:: else -> skip
							fi
						fi
					ROF
				ROF
			ROF
		ROF
		;
		
// transition #16 "t16_wd__" enabled binding search
		c_code { now.i1 = now.p2_station_wait.size; } ;
		FOR (i1)
			c_code { now.i5 = now.p8_station_next.size; } ;
			FOR (i5)
				c_code {
					;
					copy_msOfINT(&now.p2_station_wait, &fT16_in1, now.i1);
					v_id = fT16_in1;
					fT16_in2 = now.p4_env;
					v_I_src = fT16_in2.field_1;
					v_I_type = fT16_in2.field_3;
					v_I_param = fT16_in2.field_4;
					fT16_in3 = now.p16_g_cnt;
					v_g_cnt = fT16_in3;
					fT16_in4 = now.p15_g_dst;
					v_g_dst = fT16_in4;
					copy_msOfLOCALINT(&now.p8_station_next, &fT16_in5, now.i5);
					v_station_next = fT16_in5.field_2;
				} ;
				if
				:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
				:: else ->
					
					c_code {
						now.evaluationFail = now.exprError = false;
						tmp_SIGTYPE[1] = v_D;
						tmp_SIGTYPE[2] = v_I_type;
						tmp_BOOL[0] = ! cmp_SIGTYPE(&tmp_SIGTYPE[1], &tmp_SIGTYPE[2]);
						tmp_INT[4] = v_id;
						tmp_INT[5] = v_g_dst;
						tmp_BOOL[3] = ! cmp_INT(&tmp_INT[4], &tmp_INT[5]);
						now.guard = tmp_BOOL[0] && tmp_BOOL[3];
						if (! now.guard) {
							goto t16_check;
						}
						fT16_in_v1 = v_id;
						if (cmp_INT(&fT16_in1, &fT16_in_v1)) {
							now.guard = false;
							goto t16_check;
						}
						r_p2_station_wait.size = 0;
						for (i = 0; i < now.p2_station_wait.size; ++i) {
							if (i != now.i1) {
								put_msOfINT(&r_p2_station_wait, &now.p2_station_wait.content[i]);
							}
						}
						tmp_INT[0] = v_I_src;
						fT16_in_v2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_id;
						fT16_in_v2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_I_type;
						fT16_in_v2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = v_I_param;
						fT16_in_v2.field_4 = tmp_INT[3];
						if (cmp_SIGNAL(&fT16_in2, &fT16_in_v2)) {
							now.guard = false;
							goto t16_check;
						}
						fT16_in_v3 = v_g_cnt;
						if (cmp_INT(&fT16_in3, &fT16_in_v3)) {
							now.guard = false;
							goto t16_check;
						}
						fT16_in_v4 = v_g_dst;
						if (cmp_INT(&fT16_in4, &fT16_in_v4)) {
							now.guard = false;
							goto t16_check;
						}
						tmp_INT[0] = v_id;
						fT16_in_v5.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT16_in_v5.field_2 = tmp_INT[1];
						if (cmp_LOCALINT(&fT16_in5, &fT16_in_v5)) {
							now.guard = false;
							goto t16_check;
						}
						r_p8_station_next.size = 0;
						for (i = 0; i < now.p8_station_next.size; ++i) {
							if (i != now.i5) {
								put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
							}
						}
						fT16_out1 = v_id;
						if (r_p2_station_wait.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t16_check;
						}
						tmp_INT[0] = v_id;
						fT16_out2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT16_out2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_E;
						fT16_out2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = 0;
						fT16_out2.field_4 = tmp_INT[3];
						tmp_INT[0] = v_g_cnt;
						tmp_INT[1] = 1;
						fT16_out3 = tmp_INT[0] - tmp_INT[1];
						fT16_out4 = 0;
						tmp_INT[0] = v_id;
						fT16_out5.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT16_out5.field_2 = tmp_INT[1];
						if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t16_check;
						}
t16_check:
						if (now.evaluationFail) now.guard = false;
					} ;
					assert( c_expr { ! now.evaluationFail } );
					if
					:: c_expr { now.guard } ->
						final = 0;
						trans = 16;
						NONDETERM_TRANS
					:: else -> skip
					fi
				fi
			ROF
		ROF
		;
		
// transition #17 "t17_wdn" enabled binding search
		c_code { now.i1 = now.p2_station_wait.size; } ;
		FOR (i1)
			c_code { now.i4 = now.p8_station_next.size; } ;
			FOR (i4)
				c_code {
					;
					copy_msOfINT(&now.p2_station_wait, &fT17_in1, now.i1);
					v_id = fT17_in1;
					fT17_in2 = now.p4_env;
					v_I_src = fT17_in2.field_1;
					v_I_type = fT17_in2.field_3;
					v_I_param = fT17_in2.field_4;
					fT17_in3 = now.p15_g_dst;
					v_g_dst = fT17_in3;
					copy_msOfLOCALINT(&now.p8_station_next, &fT17_in4, now.i4);
					v_station_next = fT17_in4.field_2;
				} ;
				if
				:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
				:: else ->
					
					c_code {
						now.evaluationFail = now.exprError = false;
						tmp_SIGTYPE[1] = v_D;
						tmp_SIGTYPE[2] = v_I_type;
						tmp_BOOL[0] = ! cmp_SIGTYPE(&tmp_SIGTYPE[1], &tmp_SIGTYPE[2]);
						tmp_INT[4] = v_id;
						tmp_INT[5] = v_g_dst;
						tmp_BOOL[3] = cmp_INT(&tmp_INT[4], &tmp_INT[5]);
						now.guard = tmp_BOOL[0] && tmp_BOOL[3];
						if (! now.guard) {
							goto t17_check;
						}
						fT17_in_v1 = v_id;
						if (cmp_INT(&fT17_in1, &fT17_in_v1)) {
							now.guard = false;
							goto t17_check;
						}
						r_p2_station_wait.size = 0;
						for (i = 0; i < now.p2_station_wait.size; ++i) {
							if (i != now.i1) {
								put_msOfINT(&r_p2_station_wait, &now.p2_station_wait.content[i]);
							}
						}
						tmp_INT[0] = v_I_src;
						fT17_in_v2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_id;
						fT17_in_v2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_I_type;
						fT17_in_v2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = v_I_param;
						fT17_in_v2.field_4 = tmp_INT[3];
						if (cmp_SIGNAL(&fT17_in2, &fT17_in_v2)) {
							now.guard = false;
							goto t17_check;
						}
						fT17_in_v3 = v_g_dst;
						if (cmp_INT(&fT17_in3, &fT17_in_v3)) {
							now.guard = false;
							goto t17_check;
						}
						tmp_INT[0] = v_id;
						fT17_in_v4.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT17_in_v4.field_2 = tmp_INT[1];
						if (cmp_LOCALINT(&fT17_in4, &fT17_in_v4)) {
							now.guard = false;
							goto t17_check;
						}
						r_p8_station_next.size = 0;
						for (i = 0; i < now.p8_station_next.size; ++i) {
							if (i != now.i4) {
								put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
							}
						}
						fT17_out1 = v_id;
						if (r_p2_station_wait.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t17_check;
						}
						tmp_INT[0] = v_id;
						fT17_out2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT17_out2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_D;
						fT17_out2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = 0;
						fT17_out2.field_4 = tmp_INT[3];
						fT17_out3 = v_g_dst;
						tmp_INT[0] = v_id;
						fT17_out4.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT17_out4.field_2 = tmp_INT[1];
						if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t17_check;
						}
t17_check:
						if (now.evaluationFail) now.guard = false;
					} ;
					assert( c_expr { ! now.evaluationFail } );
					if
					:: c_expr { now.guard } ->
						final = 0;
						trans = 17;
						NONDETERM_TRANS
					:: else -> skip
					fi
				fi
			ROF
		ROF
		;
		
// transition #18 "t18_we__" enabled binding search
		c_code { now.i1 = now.p2_station_wait.size; } ;
		FOR (i1)
			c_code { now.i4 = now.p8_station_next.size; } ;
			FOR (i4)
				c_code {
					;
					copy_msOfINT(&now.p2_station_wait, &fT18_in1, now.i1);
					v_id = fT18_in1;
					fT18_in2 = now.p4_env;
					v_I_src = fT18_in2.field_1;
					v_I_type = fT18_in2.field_3;
					v_I_param = fT18_in2.field_4;
					fT18_in3 = now.p14_g_ba;
					v_g_ba = fT18_in3;
					copy_msOfLOCALINT(&now.p8_station_next, &fT18_in4, now.i4);
					v_station_next = fT18_in4.field_2;
				} ;
				if
				:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
				:: else ->
					
					c_code {
						now.evaluationFail = now.exprError = false;
						tmp_SIGTYPE[1] = v_E;
						tmp_SIGTYPE[2] = v_I_type;
						tmp_BOOL[0] = ! cmp_SIGTYPE(&tmp_SIGTYPE[1], &tmp_SIGTYPE[2]);
						tmp_INT[4] = v_id;
						tmp_INT[5] = v_g_ba;
						tmp_BOOL[3] = ! cmp_INT(&tmp_INT[4], &tmp_INT[5]);
						now.guard = tmp_BOOL[0] && tmp_BOOL[3];
						if (! now.guard) {
							goto t18_check;
						}
						fT18_in_v1 = v_id;
						if (cmp_INT(&fT18_in1, &fT18_in_v1)) {
							now.guard = false;
							goto t18_check;
						}
						tmp_INT[0] = v_I_src;
						fT18_in_v2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_id;
						fT18_in_v2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_I_type;
						fT18_in_v2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = v_I_param;
						fT18_in_v2.field_4 = tmp_INT[3];
						if (cmp_SIGNAL(&fT18_in2, &fT18_in_v2)) {
							now.guard = false;
							goto t18_check;
						}
						fT18_in_v3 = v_g_ba;
						if (cmp_INT(&fT18_in3, &fT18_in_v3)) {
							now.guard = false;
							goto t18_check;
						}
						tmp_INT[0] = v_id;
						fT18_in_v4.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT18_in_v4.field_2 = tmp_INT[1];
						if (cmp_LOCALINT(&fT18_in4, &fT18_in_v4)) {
							now.guard = false;
							goto t18_check;
						}
						r_p8_station_next.size = 0;
						for (i = 0; i < now.p8_station_next.size; ++i) {
							if (i != now.i4) {
								put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
							}
						}
						fT18_out1 = v_id;
						if (now.p1_station_reset.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t18_check;
						}
						tmp_INT[0] = v_id;
						fT18_out2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT18_out2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_R;
						fT18_out2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = 0;
						fT18_out2.field_4 = tmp_INT[3];
						fT18_out3 = v_g_ba;
						tmp_INT[0] = v_id;
						fT18_out4.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT18_out4.field_2 = tmp_INT[1];
						if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t18_check;
						}
t18_check:
						if (now.evaluationFail) now.guard = false;
					} ;
					assert( c_expr { ! now.evaluationFail } );
					if
					:: c_expr { now.guard } ->
						final = 0;
						trans = 18;
						NONDETERM_TRANS
					:: else -> skip
					fi
				fi
			ROF
		ROF
		;
		
// transition #19 "t19_wen" enabled binding search
		c_code { now.i1 = now.p2_station_wait.size; } ;
		FOR (i1)
			c_code { now.i4 = now.p8_station_next.size; } ;
			FOR (i4)
				c_code {
					;
					copy_msOfINT(&now.p2_station_wait, &fT19_in1, now.i1);
					v_id = fT19_in1;
					fT19_in2 = now.p4_env;
					v_I_src = fT19_in2.field_1;
					v_I_type = fT19_in2.field_3;
					v_I_param = fT19_in2.field_4;
					fT19_in3 = now.p14_g_ba;
					v_g_ba = fT19_in3;
					copy_msOfLOCALINT(&now.p8_station_next, &fT19_in4, now.i4);
					v_station_next = fT19_in4.field_2;
				} ;
				if
				:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
				:: else ->
					
					c_code {
						now.evaluationFail = now.exprError = false;
						tmp_SIGTYPE[1] = v_E;
						tmp_SIGTYPE[2] = v_I_type;
						tmp_BOOL[0] = ! cmp_SIGTYPE(&tmp_SIGTYPE[1], &tmp_SIGTYPE[2]);
						tmp_INT[4] = v_id;
						tmp_INT[5] = v_g_ba;
						tmp_BOOL[3] = cmp_INT(&tmp_INT[4], &tmp_INT[5]);
						now.guard = tmp_BOOL[0] && tmp_BOOL[3];
						if (! now.guard) {
							goto t19_check;
						}
						fT19_in_v1 = v_id;
						if (cmp_INT(&fT19_in1, &fT19_in_v1)) {
							now.guard = false;
							goto t19_check;
						}
						r_p2_station_wait.size = 0;
						for (i = 0; i < now.p2_station_wait.size; ++i) {
							if (i != now.i1) {
								put_msOfINT(&r_p2_station_wait, &now.p2_station_wait.content[i]);
							}
						}
						tmp_INT[0] = v_I_src;
						fT19_in_v2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_id;
						fT19_in_v2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_I_type;
						fT19_in_v2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = v_I_param;
						fT19_in_v2.field_4 = tmp_INT[3];
						if (cmp_SIGNAL(&fT19_in2, &fT19_in_v2)) {
							now.guard = false;
							goto t19_check;
						}
						fT19_in_v3 = v_g_ba;
						if (cmp_INT(&fT19_in3, &fT19_in_v3)) {
							now.guard = false;
							goto t19_check;
						}
						tmp_INT[0] = v_id;
						fT19_in_v4.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT19_in_v4.field_2 = tmp_INT[1];
						if (cmp_LOCALINT(&fT19_in4, &fT19_in_v4)) {
							now.guard = false;
							goto t19_check;
						}
						r_p8_station_next.size = 0;
						for (i = 0; i < now.p8_station_next.size; ++i) {
							if (i != now.i4) {
								put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
							}
						}
						fT19_out1 = v_id;
						if (r_p2_station_wait.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t19_check;
						}
						tmp_INT[0] = v_id;
						fT19_out2.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT19_out2.field_2 = tmp_INT[1];
						tmp_SIGTYPE[2] = v_E;
						fT19_out2.field_3 = tmp_SIGTYPE[2];
						tmp_INT[3] = 0;
						fT19_out2.field_4 = tmp_INT[3];
						fT19_out3 = v_g_ba;
						tmp_INT[0] = v_id;
						fT19_out4.field_1 = tmp_INT[0];
						tmp_INT[1] = v_station_next;
						fT19_out4.field_2 = tmp_INT[1];
						if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
							now.evaluationFail = true;
							goto t19_check;
						}
t19_check:
						if (now.evaluationFail) now.guard = false;
					} ;
					assert( c_expr { ! now.evaluationFail } );
					if
					:: c_expr { now.guard } ->
						final = 0;
						trans = 19;
						NONDETERM_TRANS
					:: else -> skip
					fi
				fi
			ROF
		ROF
		;
		
// transition #20 "t20_wr" enabled binding search
		c_code { now.i1 = now.p2_station_wait.size; } ;
		FOR (i1)
			c_code { now.i3 = now.p7_station_c.size; } ;
			FOR (i3)
				c_code { now.i4 = now.p11_station_maxc.size; } ;
				FOR (i4)
					c_code { now.i5 = now.p8_station_next.size; } ;
					FOR (i5)
						c_code {
							;
							copy_msOfINT(&now.p2_station_wait, &fT20_in1, now.i1);
							v_id = fT20_in1;
							fT20_in2 = now.p4_env;
							v_I_src = fT20_in2.field_1;
							v_I_type = fT20_in2.field_3;
							v_I_param = fT20_in2.field_4;
							copy_msOfLOCALINT(&now.p7_station_c, &fT20_in3, now.i3);
							v_station_c = fT20_in3.field_2;
							copy_msOfLOCALINT(&now.p11_station_maxc, &fT20_in4, now.i4);
							v_station_maxc = fT20_in4.field_2;
							copy_msOfLOCALINT(&now.p8_station_next, &fT20_in5, now.i5);
							v_station_next = fT20_in5.field_2;
						} ;
						if
						:: c_expr { now.bindingFail } -> c_code { now.bindingFail = false; }
						:: else ->
							
							c_code {
								now.evaluationFail = now.exprError = false;
								tmp_SIGTYPE[0] = v_R;
								tmp_SIGTYPE[1] = v_I_type;
								now.guard = ! cmp_SIGTYPE(&tmp_SIGTYPE[0], &tmp_SIGTYPE[1]);
								if (! now.guard) {
									goto t20_check;
								}
								fT20_in_v1 = v_id;
								if (cmp_INT(&fT20_in1, &fT20_in_v1)) {
									now.guard = false;
									goto t20_check;
								}
								tmp_INT[0] = v_I_src;
								fT20_in_v2.field_1 = tmp_INT[0];
								tmp_INT[1] = v_id;
								fT20_in_v2.field_2 = tmp_INT[1];
								tmp_SIGTYPE[2] = v_I_type;
								fT20_in_v2.field_3 = tmp_SIGTYPE[2];
								tmp_INT[3] = v_I_param;
								fT20_in_v2.field_4 = tmp_INT[3];
								if (cmp_SIGNAL(&fT20_in2, &fT20_in_v2)) {
									now.guard = false;
									goto t20_check;
								}
								tmp_INT[0] = v_id;
								fT20_in_v3.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_c;
								fT20_in_v3.field_2 = tmp_INT[1];
								if (cmp_LOCALINT(&fT20_in3, &fT20_in_v3)) {
									now.guard = false;
									goto t20_check;
								}
								r_p7_station_c.size = 0;
								for (i = 0; i < now.p7_station_c.size; ++i) {
									if (i != now.i3) {
										put_msOfLOCALINT(&r_p7_station_c, &now.p7_station_c.content[i]);
									}
								}
								tmp_INT[0] = v_id;
								fT20_in_v4.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_maxc;
								fT20_in_v4.field_2 = tmp_INT[1];
								if (cmp_LOCALINT(&fT20_in4, &fT20_in_v4)) {
									now.guard = false;
									goto t20_check;
								}
								r_p11_station_maxc.size = 0;
								for (i = 0; i < now.p11_station_maxc.size; ++i) {
									if (i != now.i4) {
										put_msOfLOCALINT(&r_p11_station_maxc, &now.p11_station_maxc.content[i]);
									}
								}
								tmp_INT[0] = v_id;
								fT20_in_v5.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_next;
								fT20_in_v5.field_2 = tmp_INT[1];
								if (cmp_LOCALINT(&fT20_in5, &fT20_in_v5)) {
									now.guard = false;
									goto t20_check;
								}
								r_p8_station_next.size = 0;
								for (i = 0; i < now.p8_station_next.size; ++i) {
									if (i != now.i5) {
										put_msOfLOCALINT(&r_p8_station_next, &now.p8_station_next.content[i]);
									}
								}
								fT20_out1 = v_id;
								if (now.p17_station_idle.size + 1 > MS_MAX_SIZE) {
									now.evaluationFail = true;
									goto t20_check;
								}
								tmp_INT[0] = v_id;
								fT20_out2.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_next;
								fT20_out2.field_2 = tmp_INT[1];
								tmp_SIGTYPE[2] = v_R;
								fT20_out2.field_3 = tmp_SIGTYPE[2];
								tmp_INT[3] = 0;
								fT20_out2.field_4 = tmp_INT[3];
								tmp_INT[0] = v_id;
								fT20_out3.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_maxc;
								fT20_out3.field_2 = tmp_INT[1];
								if (r_p7_station_c.size + 1 > MS_MAX_SIZE) {
									now.evaluationFail = true;
									goto t20_check;
								}
								tmp_INT[0] = v_id;
								fT20_out4.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_maxc;
								fT20_out4.field_2 = tmp_INT[1];
								if (r_p11_station_maxc.size + 1 > MS_MAX_SIZE) {
									now.evaluationFail = true;
									goto t20_check;
								}
								tmp_INT[0] = v_id;
								fT20_out5.field_1 = tmp_INT[0];
								tmp_INT[1] = v_station_next;
								fT20_out5.field_2 = tmp_INT[1];
								if (r_p8_station_next.size + 1 > MS_MAX_SIZE) {
									now.evaluationFail = true;
									goto t20_check;
								}
t20_check:
								if (now.evaluationFail) now.guard = false;
							} ;
							assert( c_expr { ! now.evaluationFail } );
							if
							:: c_expr { now.guard } ->
								final = 0;
								trans = 20;
								NONDETERM_TRANS
							:: else -> skip
							fi
						fi
					ROF
				ROF
			ROF
		ROF
		;
		
doTrans:
// dbgprnt
		c_code {
#ifndef CPN_NOPRINT
			print_t("======================== state_begin ========================");
			print_t("#");
			print_t("p1_station_reset:");
			print_t("#");
			for (dbgp_i = 0; dbgp_i < now.p1_station_reset.size; ++dbgp_i) {
				print_t(">");
				print_INT(&now.p1_station_reset.content[dbgp_i]);
				print_t("#");
			}
			print_t("p2_station_wait:");
			print_t("#");
			for (dbgp_i = 0; dbgp_i < now.p2_station_wait.size; ++dbgp_i) {
				print_t(">");
				print_INT(&now.p2_station_wait.content[dbgp_i]);
				print_t("#");
			}
			print_t("p3_station_send:");
			print_t("#");
			for (dbgp_i = 0; dbgp_i < now.p3_station_send.size; ++dbgp_i) {
				print_t(">");
				print_INT(&now.p3_station_send.content[dbgp_i]);
				print_t("#");
			}
			print_t("p4_env:");
			print_t("#");
			print_t(">");
			print_SIGNAL(&now.p4_env);
			print_t("#");
			print_t("p5_station_req:");
			print_t("#");
			for (dbgp_i = 0; dbgp_i < now.p5_station_req.size; ++dbgp_i) {
				print_t(">");
				print_LOCALINT(&now.p5_station_req.content[dbgp_i]);
				print_t("#");
			}
			print_t("p6_station_msgs:");
			print_t("#");
			for (dbgp_i = 0; dbgp_i < now.p6_station_msgs.size; ++dbgp_i) {
				print_t(">");
				print_LOCALINT(&now.p6_station_msgs.content[dbgp_i]);
				print_t("#");
			}
			print_t("p7_station_c:");
			print_t("#");
			for (dbgp_i = 0; dbgp_i < now.p7_station_c.size; ++dbgp_i) {
				print_t(">");
				print_LOCALINT(&now.p7_station_c.content[dbgp_i]);
				print_t("#");
			}
			print_t("p8_station_next:");
			print_t("#");
			for (dbgp_i = 0; dbgp_i < now.p8_station_next.size; ++dbgp_i) {
				print_t(">");
				print_LOCALINT(&now.p8_station_next.content[dbgp_i]);
				print_t("#");
			}
			print_t("p9_station_other:");
			print_t("#");
			for (dbgp_i = 0; dbgp_i < now.p9_station_other.size; ++dbgp_i) {
				print_t(">");
				print_LOCALARRAYINT(&now.p9_station_other.content[dbgp_i]);
				print_t("#");
			}
			print_t("p10_station_ack:");
			print_t("#");
			for (dbgp_i = 0; dbgp_i < now.p10_station_ack.size; ++dbgp_i) {
				print_t(">");
				print_LOCALINT(&now.p10_station_ack.content[dbgp_i]);
				print_t("#");
			}
			print_t("p11_station_maxc:");
			print_t("#");
			for (dbgp_i = 0; dbgp_i < now.p11_station_maxc.size; ++dbgp_i) {
				print_t(">");
				print_LOCALINT(&now.p11_station_maxc.content[dbgp_i]);
				print_t("#");
			}
			print_t("p12_station_maxmsgs:");
			print_t("#");
			for (dbgp_i = 0; dbgp_i < now.p12_station_maxmsgs.size; ++dbgp_i) {
				print_t(">");
				print_LOCALINT(&now.p12_station_maxmsgs.content[dbgp_i]);
				print_t("#");
			}
			print_t("p13_NonDeterm0:");
			print_t("#");
			for (dbgp_i = 0; dbgp_i < now.p13_NonDeterm0.size; ++dbgp_i) {
				print_t(">");
				print_INT(&now.p13_NonDeterm0.content[dbgp_i]);
				print_t("#");
			}
			print_t("p14_g_ba:");
			print_t("#");
			print_t(">");
			print_INT(&now.p14_g_ba);
			print_t("#");
			print_t("p15_g_dst:");
			print_t("#");
			print_t(">");
			print_INT(&now.p15_g_dst);
			print_t("#");
			print_t("p16_g_cnt:");
			print_t("#");
			print_t(">");
			print_INT(&now.p16_g_cnt);
			print_t("#");
			print_t("p17_station_idle:");
			print_t("#");
			for (dbgp_i = 0; dbgp_i < now.p17_station_idle.size; ++dbgp_i) {
				print_t(">");
				print_INT(&now.p17_station_idle.content[dbgp_i]);
				print_t("#");
			}
			print_t("======================== state_end ========================");
			print_t("#");
#endif
			;
		} ;
		if
// transition #1 "t1_i__s" performing
		:: trans == 1 ->
			c_code {
				get_msOfINT(&now.p17_station_idle, &fT1_in1, now.i1);
				v_id = fT1_in1;
				get_msOfLOCALINT(&now.p7_station_c, &fT1_in2, now.i2);
				v_station_c = fT1_in2.field_2;
				get_msOfLOCALINT(&now.p6_station_msgs, &fT1_in3, now.i3);
				v_station_msgs = fT1_in3.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t1_i__s");
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_c:");
				print_INT(&v_station_c);
				print_t("#");
				print_t(">");
				print_t("station_msgs:");
				print_INT(&v_station_msgs);
				print_t("#");
				print_t("");
#endif
				fT1_out1 = v_id;
				put_msOfINT(&now.p3_station_send, &fT1_out1);
				tmp_INT[0] = v_id;
				fT1_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_c;
				fT1_out2.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p7_station_c, &fT1_out2);
				tmp_INT[0] = v_id;
				fT1_out3.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_msgs;
				fT1_out3.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p6_station_msgs, &fT1_out3);
			} ;
			CONTINUE
			
// transition #2 "t2_i__w" performing
		:: trans == 2 ->
			c_code {
				get_msOfINT(&now.p17_station_idle, &fT2_in1, now.i1);
				v_id = fT2_in1;
				get_msOfLOCALINT(&now.p7_station_c, &fT2_in2, now.i2);
				v_station_c = fT2_in2.field_2;
				get_msOfLOCALINT(&now.p6_station_msgs, &fT2_in3, now.i3);
				v_station_msgs = fT2_in3.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t2_i__w");
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_c:");
				print_INT(&v_station_c);
				print_t("#");
				print_t(">");
				print_t("station_msgs:");
				print_INT(&v_station_msgs);
				print_t("#");
				print_t("");
#endif
				fT2_out1 = v_id;
				put_msOfINT(&now.p2_station_wait, &fT2_out1);
				tmp_INT[0] = v_id;
				fT2_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_c;
				fT2_out2.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p7_station_c, &fT2_out2);
				tmp_INT[0] = v_id;
				fT2_out3.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_msgs;
				fT2_out3.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p6_station_msgs, &fT2_out3);
			} ;
			CONTINUE
			
// transition #3 "t3_id__" performing
		:: trans == 3 ->
			c_code {
				get_msOfINT(&now.p17_station_idle, &fT3_in1, now.i1);
				v_id = fT3_in1;
				fT3_in2 = now.p4_env;
				v_I_src = fT3_in2.field_1;
				v_I_type = fT3_in2.field_3;
				v_I_param = fT3_in2.field_4;
				fT3_in3 = now.p16_g_cnt;
				v_g_cnt = fT3_in3;
				fT3_in4 = now.p15_g_dst;
				v_g_dst = fT3_in4;
				get_msOfLOCALINT(&now.p10_station_ack, &fT3_in5, now.i5);
				v_station_ack = fT3_in5.field_2;
				get_msOfLOCALINT(&now.p12_station_maxmsgs, &fT3_in6, now.i6);
				v_station_maxmsgs = fT3_in6.field_2;
				get_msOfLOCALINT(&now.p6_station_msgs, &fT3_in7, now.i7);
				v_station_msgs = fT3_in7.field_2;
				get_msOfLOCALINT(&now.p8_station_next, &fT3_in8, now.i8);
				v_station_next = fT3_in8.field_2;
				get_msOfLOCALINT(&now.p5_station_req, &fT3_in9, now.i9);
				v_station_req = fT3_in9.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t3_id__");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("g_cnt:");
				print_INT(&v_g_cnt);
				print_t("#");
				print_t(">");
				print_t("g_dst:");
				print_INT(&v_g_dst);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_ack:");
				print_INT(&v_station_ack);
				print_t("#");
				print_t(">");
				print_t("station_maxmsgs:");
				print_INT(&v_station_maxmsgs);
				print_t("#");
				print_t(">");
				print_t("station_msgs:");
				print_INT(&v_station_msgs);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t(">");
				print_t("station_req:");
				print_INT(&v_station_req);
				print_t("#");
				print_t("");
#endif
				fT3_out1 = v_id;
				put_msOfINT(&now.p17_station_idle, &fT3_out1);
				tmp_INT[0] = v_id;
				fT3_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT3_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_E;
				fT3_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT3_out2.field_4 = tmp_INT[3];
				now.p4_env = fT3_out2;
				tmp_INT[0] = v_g_cnt;
				tmp_INT[1] = 1;
				fT3_out3 = tmp_INT[0] - tmp_INT[1];
				now.p16_g_cnt = fT3_out3;
				fT3_out4 = 0;
				now.p15_g_dst = fT3_out4;
				tmp_INT[0] = v_id;
				fT3_out5.field_1 = tmp_INT[0];
				tmp_INT[1] = 0;
				fT3_out5.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p10_station_ack, &fT3_out5);
				tmp_INT[0] = v_id;
				fT3_out6.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxmsgs;
				fT3_out6.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p12_station_maxmsgs, &fT3_out6);
				tmp_INT[0] = v_id;
				fT3_out7.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxmsgs;
				fT3_out7.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p6_station_msgs, &fT3_out7);
				tmp_INT[0] = v_id;
				fT3_out8.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT3_out8.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT3_out8);
				tmp_INT[0] = v_id;
				fT3_out9.field_1 = tmp_INT[0];
				tmp_INT[1] = 1;
				fT3_out9.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p5_station_req, &fT3_out9);
			} ;
			CONTINUE
			
// transition #4 "t4_idn" performing
		:: trans == 4 ->
			c_code {
				get_msOfINT(&now.p17_station_idle, &fT4_in1, now.i1);
				v_id = fT4_in1;
				fT4_in2 = now.p4_env;
				v_I_src = fT4_in2.field_1;
				v_I_type = fT4_in2.field_3;
				v_I_param = fT4_in2.field_4;
				fT4_in3 = now.p15_g_dst;
				v_g_dst = fT4_in3;
				get_msOfLOCALINT(&now.p10_station_ack, &fT4_in4, now.i4);
				v_station_ack = fT4_in4.field_2;
				get_msOfLOCALINT(&now.p12_station_maxmsgs, &fT4_in5, now.i5);
				v_station_maxmsgs = fT4_in5.field_2;
				get_msOfLOCALINT(&now.p6_station_msgs, &fT4_in6, now.i6);
				v_station_msgs = fT4_in6.field_2;
				get_msOfLOCALINT(&now.p8_station_next, &fT4_in7, now.i7);
				v_station_next = fT4_in7.field_2;
				get_msOfLOCALINT(&now.p5_station_req, &fT4_in8, now.i8);
				v_station_req = fT4_in8.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t4_idn");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("g_dst:");
				print_INT(&v_g_dst);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_ack:");
				print_INT(&v_station_ack);
				print_t("#");
				print_t(">");
				print_t("station_maxmsgs:");
				print_INT(&v_station_maxmsgs);
				print_t("#");
				print_t(">");
				print_t("station_msgs:");
				print_INT(&v_station_msgs);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t(">");
				print_t("station_req:");
				print_INT(&v_station_req);
				print_t("#");
				print_t("");
#endif
				fT4_out1 = v_id;
				put_msOfINT(&now.p17_station_idle, &fT4_out1);
				tmp_INT[0] = v_id;
				fT4_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT4_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_D;
				fT4_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT4_out2.field_4 = tmp_INT[3];
				now.p4_env = fT4_out2;
				fT4_out3 = v_g_dst;
				now.p15_g_dst = fT4_out3;
				tmp_INT[0] = v_id;
				fT4_out4.field_1 = tmp_INT[0];
				tmp_INT[1] = 0;
				fT4_out4.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p10_station_ack, &fT4_out4);
				tmp_INT[0] = v_id;
				fT4_out5.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxmsgs;
				fT4_out5.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p12_station_maxmsgs, &fT4_out5);
				tmp_INT[0] = v_id;
				fT4_out6.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxmsgs;
				fT4_out6.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p6_station_msgs, &fT4_out6);
				tmp_INT[0] = v_id;
				fT4_out7.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT4_out7.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT4_out7);
				tmp_INT[0] = v_id;
				fT4_out8.field_1 = tmp_INT[0];
				tmp_INT[1] = 1;
				fT4_out8.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p5_station_req, &fT4_out8);
			} ;
			CONTINUE
			
// transition #5 "t5_ie__" performing
		:: trans == 5 ->
			c_code {
				get_msOfINT(&now.p17_station_idle, &fT5_in1, now.i1);
				v_id = fT5_in1;
				fT5_in2 = now.p4_env;
				v_I_src = fT5_in2.field_1;
				v_I_type = fT5_in2.field_3;
				v_I_param = fT5_in2.field_4;
				fT5_in3 = now.p14_g_ba;
				v_g_ba = fT5_in3;
				get_msOfLOCALINT(&now.p10_station_ack, &fT5_in4, now.i4);
				v_station_ack = fT5_in4.field_2;
				get_msOfLOCALINT(&now.p12_station_maxmsgs, &fT5_in5, now.i5);
				v_station_maxmsgs = fT5_in5.field_2;
				get_msOfLOCALINT(&now.p6_station_msgs, &fT5_in6, now.i6);
				v_station_msgs = fT5_in6.field_2;
				get_msOfLOCALINT(&now.p8_station_next, &fT5_in7, now.i7);
				v_station_next = fT5_in7.field_2;
				get_msOfLOCALINT(&now.p5_station_req, &fT5_in8, now.i8);
				v_station_req = fT5_in8.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t5_ie__");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("g_ba:");
				print_INT(&v_g_ba);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_ack:");
				print_INT(&v_station_ack);
				print_t("#");
				print_t(">");
				print_t("station_maxmsgs:");
				print_INT(&v_station_maxmsgs);
				print_t("#");
				print_t(">");
				print_t("station_msgs:");
				print_INT(&v_station_msgs);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t(">");
				print_t("station_req:");
				print_INT(&v_station_req);
				print_t("#");
				print_t("");
#endif
				fT5_out1 = v_id;
				put_msOfINT(&now.p1_station_reset, &fT5_out1);
				tmp_INT[0] = v_id;
				fT5_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT5_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_R;
				fT5_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT5_out2.field_4 = tmp_INT[3];
				now.p4_env = fT5_out2;
				fT5_out3 = v_g_ba;
				now.p14_g_ba = fT5_out3;
				tmp_INT[0] = v_id;
				fT5_out4.field_1 = tmp_INT[0];
				tmp_INT[1] = 0;
				fT5_out4.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p10_station_ack, &fT5_out4);
				tmp_INT[0] = v_id;
				fT5_out5.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxmsgs;
				fT5_out5.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p12_station_maxmsgs, &fT5_out5);
				tmp_INT[0] = v_id;
				fT5_out6.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxmsgs;
				fT5_out6.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p6_station_msgs, &fT5_out6);
				tmp_INT[0] = v_id;
				fT5_out7.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT5_out7.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT5_out7);
				tmp_INT[0] = v_id;
				fT5_out8.field_1 = tmp_INT[0];
				tmp_INT[1] = 1;
				fT5_out8.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p5_station_req, &fT5_out8);
			} ;
			CONTINUE
			
// transition #6 "t6_ien" performing
		:: trans == 6 ->
			c_code {
				get_msOfINT(&now.p17_station_idle, &fT6_in1, now.i1);
				v_id = fT6_in1;
				fT6_in2 = now.p4_env;
				v_I_src = fT6_in2.field_1;
				v_I_type = fT6_in2.field_3;
				v_I_param = fT6_in2.field_4;
				fT6_in3 = now.p14_g_ba;
				v_g_ba = fT6_in3;
				get_msOfLOCALINT(&now.p10_station_ack, &fT6_in4, now.i4);
				v_station_ack = fT6_in4.field_2;
				get_msOfLOCALINT(&now.p12_station_maxmsgs, &fT6_in5, now.i5);
				v_station_maxmsgs = fT6_in5.field_2;
				get_msOfLOCALINT(&now.p6_station_msgs, &fT6_in6, now.i6);
				v_station_msgs = fT6_in6.field_2;
				get_msOfLOCALINT(&now.p8_station_next, &fT6_in7, now.i7);
				v_station_next = fT6_in7.field_2;
				get_msOfLOCALINT(&now.p5_station_req, &fT6_in8, now.i8);
				v_station_req = fT6_in8.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t6_ien");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("g_ba:");
				print_INT(&v_g_ba);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_ack:");
				print_INT(&v_station_ack);
				print_t("#");
				print_t(">");
				print_t("station_maxmsgs:");
				print_INT(&v_station_maxmsgs);
				print_t("#");
				print_t(">");
				print_t("station_msgs:");
				print_INT(&v_station_msgs);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t(">");
				print_t("station_req:");
				print_INT(&v_station_req);
				print_t("#");
				print_t("");
#endif
				fT6_out1 = v_id;
				put_msOfINT(&now.p17_station_idle, &fT6_out1);
				tmp_INT[0] = v_id;
				fT6_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT6_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_E;
				fT6_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT6_out2.field_4 = tmp_INT[3];
				now.p4_env = fT6_out2;
				fT6_out3 = v_g_ba;
				now.p14_g_ba = fT6_out3;
				tmp_INT[0] = v_id;
				fT6_out4.field_1 = tmp_INT[0];
				tmp_INT[1] = 0;
				fT6_out4.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p10_station_ack, &fT6_out4);
				tmp_INT[0] = v_id;
				fT6_out5.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxmsgs;
				fT6_out5.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p12_station_maxmsgs, &fT6_out5);
				tmp_INT[0] = v_id;
				fT6_out6.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxmsgs;
				fT6_out6.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p6_station_msgs, &fT6_out6);
				tmp_INT[0] = v_id;
				fT6_out7.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT6_out7.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT6_out7);
				tmp_INT[0] = v_id;
				fT6_out8.field_1 = tmp_INT[0];
				tmp_INT[1] = 1;
				fT6_out8.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p5_station_req, &fT6_out8);
			} ;
			CONTINUE
			
// transition #7 "t7_ir" performing
		:: trans == 7 ->
			c_code {
				get_msOfINT(&now.p17_station_idle, &fT7_in1, now.i1);
				v_id = fT7_in1;
				fT7_in2 = now.p4_env;
				v_I_src = fT7_in2.field_1;
				v_I_type = fT7_in2.field_3;
				v_I_param = fT7_in2.field_4;
				get_msOfLOCALINT(&now.p10_station_ack, &fT7_in3, now.i3);
				v_station_ack = fT7_in3.field_2;
				get_msOfLOCALINT(&now.p7_station_c, &fT7_in4, now.i4);
				v_station_c = fT7_in4.field_2;
				get_msOfLOCALINT(&now.p11_station_maxc, &fT7_in5, now.i5);
				v_station_maxc = fT7_in5.field_2;
				get_msOfLOCALINT(&now.p12_station_maxmsgs, &fT7_in6, now.i6);
				v_station_maxmsgs = fT7_in6.field_2;
				get_msOfLOCALINT(&now.p6_station_msgs, &fT7_in7, now.i7);
				v_station_msgs = fT7_in7.field_2;
				get_msOfLOCALINT(&now.p8_station_next, &fT7_in8, now.i8);
				v_station_next = fT7_in8.field_2;
				get_msOfLOCALINT(&now.p5_station_req, &fT7_in9, now.i9);
				v_station_req = fT7_in9.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t7_ir");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_ack:");
				print_INT(&v_station_ack);
				print_t("#");
				print_t(">");
				print_t("station_c:");
				print_INT(&v_station_c);
				print_t("#");
				print_t(">");
				print_t("station_maxc:");
				print_INT(&v_station_maxc);
				print_t("#");
				print_t(">");
				print_t("station_maxmsgs:");
				print_INT(&v_station_maxmsgs);
				print_t("#");
				print_t(">");
				print_t("station_msgs:");
				print_INT(&v_station_msgs);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t(">");
				print_t("station_req:");
				print_INT(&v_station_req);
				print_t("#");
				print_t("");
#endif
				fT7_out1 = v_id;
				put_msOfINT(&now.p17_station_idle, &fT7_out1);
				tmp_INT[0] = v_id;
				fT7_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT7_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_R;
				fT7_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT7_out2.field_4 = tmp_INT[3];
				now.p4_env = fT7_out2;
				tmp_INT[0] = v_id;
				fT7_out3.field_1 = tmp_INT[0];
				tmp_INT[1] = 0;
				fT7_out3.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p10_station_ack, &fT7_out3);
				tmp_INT[0] = v_id;
				fT7_out4.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxc;
				fT7_out4.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p7_station_c, &fT7_out4);
				tmp_INT[0] = v_id;
				fT7_out5.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxc;
				fT7_out5.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p11_station_maxc, &fT7_out5);
				tmp_INT[0] = v_id;
				fT7_out6.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxmsgs;
				fT7_out6.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p12_station_maxmsgs, &fT7_out6);
				tmp_INT[0] = v_id;
				fT7_out7.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxmsgs;
				fT7_out7.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p6_station_msgs, &fT7_out7);
				tmp_INT[0] = v_id;
				fT7_out8.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT7_out8.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT7_out8);
				tmp_INT[0] = v_id;
				fT7_out9.field_1 = tmp_INT[0];
				tmp_INT[1] = 1;
				fT7_out9.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p5_station_req, &fT7_out9);
			} ;
			CONTINUE
			
// transition #8 "t8_sd__" performing
		:: trans == 8 ->
			c_code {
				get_msOfINT(&now.p3_station_send, &fT8_in1, now.i1);
				v_id = fT8_in1;
				fT8_in2 = now.p4_env;
				v_I_src = fT8_in2.field_1;
				v_I_type = fT8_in2.field_3;
				v_I_param = fT8_in2.field_4;
				fT8_in3 = now.p14_g_ba;
				v_g_ba = fT8_in3;
				fT8_in4 = now.p16_g_cnt;
				v_g_cnt = fT8_in4;
				fT8_in5 = now.p15_g_dst;
				v_g_dst = fT8_in5;
				get_msOfLOCALINT(&now.p8_station_next, &fT8_in6, now.i6);
				v_station_next = fT8_in6.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t8_sd__");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("g_ba:");
				print_INT(&v_g_ba);
				print_t("#");
				print_t(">");
				print_t("g_cnt:");
				print_INT(&v_g_cnt);
				print_t("#");
				print_t(">");
				print_t("g_dst:");
				print_INT(&v_g_dst);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t("");
#endif
				fT8_out1 = v_id;
				put_msOfINT(&now.p3_station_send, &fT8_out1);
				tmp_INT[0] = v_id;
				fT8_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT8_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_E;
				fT8_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT8_out2.field_4 = tmp_INT[3];
				now.p4_env = fT8_out2;
				fT8_out3 = v_id;
				now.p14_g_ba = fT8_out3;
				tmp_INT[0] = v_g_cnt;
				tmp_INT[1] = 1;
				fT8_out4 = tmp_INT[0] - tmp_INT[1];
				now.p16_g_cnt = fT8_out4;
				fT8_out5 = 0;
				now.p15_g_dst = fT8_out5;
				tmp_INT[0] = v_id;
				fT8_out6.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT8_out6.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT8_out6);
			} ;
			CONTINUE
			
// transition #9 "t9_sdn" performing
		:: trans == 9 ->
			c_code {
				get_msOfINT(&now.p3_station_send, &fT9_in1, now.i1);
				v_id = fT9_in1;
				fT9_in2 = now.p4_env;
				v_I_src = fT9_in2.field_1;
				v_I_type = fT9_in2.field_3;
				v_I_param = fT9_in2.field_4;
				fT9_in3 = now.p14_g_ba;
				v_g_ba = fT9_in3;
				fT9_in4 = now.p15_g_dst;
				v_g_dst = fT9_in4;
				get_msOfLOCALINT(&now.p8_station_next, &fT9_in5, now.i5);
				v_station_next = fT9_in5.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t9_sdn");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("g_ba:");
				print_INT(&v_g_ba);
				print_t("#");
				print_t(">");
				print_t("g_dst:");
				print_INT(&v_g_dst);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t("");
#endif
				fT9_out1 = v_id;
				put_msOfINT(&now.p3_station_send, &fT9_out1);
				tmp_INT[0] = v_id;
				fT9_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT9_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_D;
				fT9_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT9_out2.field_4 = tmp_INT[3];
				now.p4_env = fT9_out2;
				fT9_out3 = v_id;
				now.p14_g_ba = fT9_out3;
				fT9_out4 = v_g_dst;
				now.p15_g_dst = fT9_out4;
				tmp_INT[0] = v_id;
				fT9_out5.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT9_out5.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT9_out5);
			} ;
			CONTINUE
			
// transition #10 "t10_se" performing
		:: trans == 10 ->
			c_code {
				get_msOfINT(&now.p3_station_send, &fT10_in1, now.i1);
				v_id = fT10_in1;
				fT10_in2 = now.p4_env;
				v_I_src = fT10_in2.field_1;
				v_I_type = fT10_in2.field_3;
				v_I_param = fT10_in2.field_4;
				get_msOfINT(&now.p13_NonDeterm0, &fT10_in3, now.i3);
				v_NonDeterm0 = fT10_in3;
				fT10_in4 = now.p14_g_ba;
				v_g_ba = fT10_in4;
				fT10_in5 = now.p16_g_cnt;
				v_g_cnt = fT10_in5;
				fT10_in6 = now.p15_g_dst;
				v_g_dst = fT10_in6;
				get_msOfLOCALINT(&now.p10_station_ack, &fT10_in7, now.i7);
				v_station_ack = fT10_in7.field_2;
				get_msOfLOCALINT(&now.p7_station_c, &fT10_in8, now.i8);
				v_station_c = fT10_in8.field_2;
				get_msOfLOCALINT(&now.p6_station_msgs, &fT10_in9, now.i9);
				v_station_msgs = fT10_in9.field_2;
				get_msOfLOCALINT(&now.p8_station_next, &fT10_in10, now.i10);
				v_station_next = fT10_in10.field_2;
				get_msOfLOCALARRAYINT(&now.p9_station_other, &fT10_in11, now.i11);
				v_station_other = fT10_in11.field_2;
				get_msOfLOCALINT(&now.p5_station_req, &fT10_in12, now.i12);
				v_station_req = fT10_in12.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t10_se");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("NonDeterm0:");
				print_INT(&v_NonDeterm0);
				print_t("#");
				print_t(">");
				print_t("g_ba:");
				print_INT(&v_g_ba);
				print_t("#");
				print_t(">");
				print_t("g_cnt:");
				print_INT(&v_g_cnt);
				print_t("#");
				print_t(">");
				print_t("g_dst:");
				print_INT(&v_g_dst);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_ack:");
				print_INT(&v_station_ack);
				print_t("#");
				print_t(">");
				print_t("station_c:");
				print_INT(&v_station_c);
				print_t("#");
				print_t(">");
				print_t("station_msgs:");
				print_INT(&v_station_msgs);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t(">");
				print_t("station_other:");
				print_ARRAYINT(&v_station_other);
				print_t("#");
				print_t(">");
				print_t("station_req:");
				print_INT(&v_station_req);
				print_t("#");
				print_t("");
#endif
				fT10_out1 = v_id;
				put_msOfINT(&now.p17_station_idle, &fT10_out1);
				tmp_INT[0] = v_id;
				fT10_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT10_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_D;
				fT10_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT10_out2.field_4 = tmp_INT[3];
				now.p4_env = fT10_out2;
				fT10_out3 = v_NonDeterm0;
				put_msOfINT(&now.p13_NonDeterm0, &fT10_out3);
				fT10_out4 = v_id;
				now.p14_g_ba = fT10_out4;
				tmp_INT[0] = v_g_cnt;
				tmp_INT[1] = 1;
				fT10_out5 = tmp_INT[0] + tmp_INT[1];
				now.p16_g_cnt = fT10_out5;
				tmp_ARRAYINT[0] = v_station_other;
				tmp_INT[3] = v_NonDeterm0;
				tmp_INT[4] = 0;
				tmp_BOOL[2] = ! cmp_INT(&tmp_INT[3], &tmp_INT[4]);
				if (tmp_BOOL[2]) {
					tmp_INT[5] = 0;
					tmp_INT[1] = tmp_INT[5];
				}
				else {
					tmp_INT[6] = 1;
					tmp_INT[1] = tmp_INT[6];
				}
				nth_ARRAYINT(&fT10_out6, &tmp_ARRAYINT[0], tmp_INT[1]);
				now.p15_g_dst = fT10_out6;
				tmp_INT[0] = v_id;
				fT10_out7.field_1 = tmp_INT[0];
				tmp_INT[1] = 1;
				fT10_out7.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p10_station_ack, &fT10_out7);
				tmp_INT[0] = v_id;
				fT10_out8.field_1 = tmp_INT[0];
				tmp_INT[2] = v_station_c;
				tmp_INT[3] = 1;
				tmp_INT[1] = tmp_INT[2] - tmp_INT[3];
				fT10_out8.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p7_station_c, &fT10_out8);
				tmp_INT[0] = v_id;
				fT10_out9.field_1 = tmp_INT[0];
				tmp_INT[2] = v_station_msgs;
				tmp_INT[3] = 1;
				tmp_INT[1] = tmp_INT[2] - tmp_INT[3];
				fT10_out9.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p6_station_msgs, &fT10_out9);
				tmp_INT[0] = v_id;
				fT10_out10.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT10_out10.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT10_out10);
				tmp_INT[0] = v_id;
				fT10_out11.field_1 = tmp_INT[0];
				tmp_ARRAYINT[1] = v_station_other;
				fT10_out11.field_2 = tmp_ARRAYINT[1];
				put_msOfLOCALARRAYINT(&now.p9_station_other, &fT10_out11);
				tmp_INT[0] = v_id;
				fT10_out12.field_1 = tmp_INT[0];
				tmp_INT[1] = 0;
				fT10_out12.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p5_station_req, &fT10_out12);
			} ;
			CONTINUE
			
// transition #11 "t11_sr" performing
		:: trans == 11 ->
			c_code {
				get_msOfINT(&now.p3_station_send, &fT11_in1, now.i1);
				v_id = fT11_in1;
				fT11_in2 = now.p4_env;
				v_I_src = fT11_in2.field_1;
				v_I_type = fT11_in2.field_3;
				v_I_param = fT11_in2.field_4;
				get_msOfLOCALINT(&now.p7_station_c, &fT11_in3, now.i3);
				v_station_c = fT11_in3.field_2;
				get_msOfLOCALINT(&now.p11_station_maxc, &fT11_in4, now.i4);
				v_station_maxc = fT11_in4.field_2;
				get_msOfLOCALINT(&now.p8_station_next, &fT11_in5, now.i5);
				v_station_next = fT11_in5.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t11_sr");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_c:");
				print_INT(&v_station_c);
				print_t("#");
				print_t(">");
				print_t("station_maxc:");
				print_INT(&v_station_maxc);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t("");
#endif
				fT11_out1 = v_id;
				put_msOfINT(&now.p17_station_idle, &fT11_out1);
				tmp_INT[0] = v_id;
				fT11_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT11_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_R;
				fT11_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT11_out2.field_4 = tmp_INT[3];
				now.p4_env = fT11_out2;
				tmp_INT[0] = v_id;
				fT11_out3.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxc;
				fT11_out3.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p7_station_c, &fT11_out3);
				tmp_INT[0] = v_id;
				fT11_out4.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxc;
				fT11_out4.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p11_station_maxc, &fT11_out4);
				tmp_INT[0] = v_id;
				fT11_out5.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT11_out5.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT11_out5);
			} ;
			CONTINUE
			
// transition #12 "t12_rd__" performing
		:: trans == 12 ->
			c_code {
				get_msOfINT(&now.p1_station_reset, &fT12_in1, now.i1);
				v_id = fT12_in1;
				fT12_in2 = now.p4_env;
				v_I_src = fT12_in2.field_1;
				v_I_type = fT12_in2.field_3;
				v_I_param = fT12_in2.field_4;
				fT12_in3 = now.p16_g_cnt;
				v_g_cnt = fT12_in3;
				fT12_in4 = now.p15_g_dst;
				v_g_dst = fT12_in4;
				get_msOfLOCALINT(&now.p8_station_next, &fT12_in5, now.i5);
				v_station_next = fT12_in5.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t12_rd__");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("g_cnt:");
				print_INT(&v_g_cnt);
				print_t("#");
				print_t(">");
				print_t("g_dst:");
				print_INT(&v_g_dst);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t("");
#endif
				fT12_out1 = v_id;
				put_msOfINT(&now.p1_station_reset, &fT12_out1);
				tmp_INT[0] = v_id;
				fT12_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT12_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_E;
				fT12_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT12_out2.field_4 = tmp_INT[3];
				now.p4_env = fT12_out2;
				tmp_INT[0] = v_g_cnt;
				tmp_INT[1] = 1;
				fT12_out3 = tmp_INT[0] - tmp_INT[1];
				now.p16_g_cnt = fT12_out3;
				fT12_out4 = 0;
				now.p15_g_dst = fT12_out4;
				tmp_INT[0] = v_id;
				fT12_out5.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT12_out5.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT12_out5);
			} ;
			CONTINUE
			
// transition #13 "t13_rdn" performing
		:: trans == 13 ->
			c_code {
				get_msOfINT(&now.p1_station_reset, &fT13_in1, now.i1);
				v_id = fT13_in1;
				fT13_in2 = now.p4_env;
				v_I_src = fT13_in2.field_1;
				v_I_type = fT13_in2.field_3;
				v_I_param = fT13_in2.field_4;
				fT13_in3 = now.p15_g_dst;
				v_g_dst = fT13_in3;
				get_msOfLOCALINT(&now.p8_station_next, &fT13_in4, now.i4);
				v_station_next = fT13_in4.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t13_rdn");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("g_dst:");
				print_INT(&v_g_dst);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t("");
#endif
				fT13_out1 = v_id;
				put_msOfINT(&now.p1_station_reset, &fT13_out1);
				tmp_INT[0] = v_id;
				fT13_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT13_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_D;
				fT13_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT13_out2.field_4 = tmp_INT[3];
				now.p4_env = fT13_out2;
				fT13_out3 = v_g_dst;
				now.p15_g_dst = fT13_out3;
				tmp_INT[0] = v_id;
				fT13_out4.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT13_out4.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT13_out4);
			} ;
			CONTINUE
			
// transition #14 "t14_re" performing
		:: trans == 14 ->
			c_code {
				get_msOfINT(&now.p1_station_reset, &fT14_in1, now.i1);
				v_id = fT14_in1;
				fT14_in2 = now.p4_env;
				v_I_src = fT14_in2.field_1;
				v_I_type = fT14_in2.field_3;
				v_I_param = fT14_in2.field_4;
				get_msOfLOCALINT(&now.p8_station_next, &fT14_in3, now.i3);
				v_station_next = fT14_in3.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t14_re");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t("");
#endif
				fT14_out1 = v_id;
				put_msOfINT(&now.p1_station_reset, &fT14_out1);
				tmp_INT[0] = v_id;
				fT14_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT14_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_E;
				fT14_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT14_out2.field_4 = tmp_INT[3];
				now.p4_env = fT14_out2;
				tmp_INT[0] = v_id;
				fT14_out3.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT14_out3.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT14_out3);
			} ;
			CONTINUE
			
// transition #15 "t15_rr" performing
		:: trans == 15 ->
			c_code {
				get_msOfINT(&now.p1_station_reset, &fT15_in1, now.i1);
				v_id = fT15_in1;
				fT15_in2 = now.p4_env;
				v_I_src = fT15_in2.field_1;
				v_I_type = fT15_in2.field_3;
				v_I_param = fT15_in2.field_4;
				fT15_in3 = now.p14_g_ba;
				v_g_ba = fT15_in3;
				get_msOfLOCALINT(&now.p7_station_c, &fT15_in4, now.i4);
				v_station_c = fT15_in4.field_2;
				get_msOfLOCALINT(&now.p11_station_maxc, &fT15_in5, now.i5);
				v_station_maxc = fT15_in5.field_2;
				get_msOfLOCALINT(&now.p8_station_next, &fT15_in6, now.i6);
				v_station_next = fT15_in6.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t15_rr");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("g_ba:");
				print_INT(&v_g_ba);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_c:");
				print_INT(&v_station_c);
				print_t("#");
				print_t(">");
				print_t("station_maxc:");
				print_INT(&v_station_maxc);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t("");
#endif
				fT15_out1 = v_id;
				put_msOfINT(&now.p17_station_idle, &fT15_out1);
				tmp_INT[0] = v_id;
				fT15_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT15_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_E;
				fT15_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT15_out2.field_4 = tmp_INT[3];
				now.p4_env = fT15_out2;
				fT15_out3 = v_id;
				now.p14_g_ba = fT15_out3;
				tmp_INT[0] = v_id;
				fT15_out4.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxc;
				fT15_out4.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p7_station_c, &fT15_out4);
				tmp_INT[0] = v_id;
				fT15_out5.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxc;
				fT15_out5.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p11_station_maxc, &fT15_out5);
				tmp_INT[0] = v_id;
				fT15_out6.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT15_out6.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT15_out6);
			} ;
			CONTINUE
			
// transition #16 "t16_wd__" performing
		:: trans == 16 ->
			c_code {
				get_msOfINT(&now.p2_station_wait, &fT16_in1, now.i1);
				v_id = fT16_in1;
				fT16_in2 = now.p4_env;
				v_I_src = fT16_in2.field_1;
				v_I_type = fT16_in2.field_3;
				v_I_param = fT16_in2.field_4;
				fT16_in3 = now.p16_g_cnt;
				v_g_cnt = fT16_in3;
				fT16_in4 = now.p15_g_dst;
				v_g_dst = fT16_in4;
				get_msOfLOCALINT(&now.p8_station_next, &fT16_in5, now.i5);
				v_station_next = fT16_in5.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t16_wd__");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("g_cnt:");
				print_INT(&v_g_cnt);
				print_t("#");
				print_t(">");
				print_t("g_dst:");
				print_INT(&v_g_dst);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t("");
#endif
				fT16_out1 = v_id;
				put_msOfINT(&now.p2_station_wait, &fT16_out1);
				tmp_INT[0] = v_id;
				fT16_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT16_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_E;
				fT16_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT16_out2.field_4 = tmp_INT[3];
				now.p4_env = fT16_out2;
				tmp_INT[0] = v_g_cnt;
				tmp_INT[1] = 1;
				fT16_out3 = tmp_INT[0] - tmp_INT[1];
				now.p16_g_cnt = fT16_out3;
				fT16_out4 = 0;
				now.p15_g_dst = fT16_out4;
				tmp_INT[0] = v_id;
				fT16_out5.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT16_out5.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT16_out5);
			} ;
			CONTINUE
			
// transition #17 "t17_wdn" performing
		:: trans == 17 ->
			c_code {
				get_msOfINT(&now.p2_station_wait, &fT17_in1, now.i1);
				v_id = fT17_in1;
				fT17_in2 = now.p4_env;
				v_I_src = fT17_in2.field_1;
				v_I_type = fT17_in2.field_3;
				v_I_param = fT17_in2.field_4;
				fT17_in3 = now.p15_g_dst;
				v_g_dst = fT17_in3;
				get_msOfLOCALINT(&now.p8_station_next, &fT17_in4, now.i4);
				v_station_next = fT17_in4.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t17_wdn");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("g_dst:");
				print_INT(&v_g_dst);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t("");
#endif
				fT17_out1 = v_id;
				put_msOfINT(&now.p2_station_wait, &fT17_out1);
				tmp_INT[0] = v_id;
				fT17_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT17_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_D;
				fT17_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT17_out2.field_4 = tmp_INT[3];
				now.p4_env = fT17_out2;
				fT17_out3 = v_g_dst;
				now.p15_g_dst = fT17_out3;
				tmp_INT[0] = v_id;
				fT17_out4.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT17_out4.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT17_out4);
			} ;
			CONTINUE
			
// transition #18 "t18_we__" performing
		:: trans == 18 ->
			c_code {
				get_msOfINT(&now.p2_station_wait, &fT18_in1, now.i1);
				v_id = fT18_in1;
				fT18_in2 = now.p4_env;
				v_I_src = fT18_in2.field_1;
				v_I_type = fT18_in2.field_3;
				v_I_param = fT18_in2.field_4;
				fT18_in3 = now.p14_g_ba;
				v_g_ba = fT18_in3;
				get_msOfLOCALINT(&now.p8_station_next, &fT18_in4, now.i4);
				v_station_next = fT18_in4.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t18_we__");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("g_ba:");
				print_INT(&v_g_ba);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t("");
#endif
				fT18_out1 = v_id;
				put_msOfINT(&now.p1_station_reset, &fT18_out1);
				tmp_INT[0] = v_id;
				fT18_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT18_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_R;
				fT18_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT18_out2.field_4 = tmp_INT[3];
				now.p4_env = fT18_out2;
				fT18_out3 = v_g_ba;
				now.p14_g_ba = fT18_out3;
				tmp_INT[0] = v_id;
				fT18_out4.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT18_out4.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT18_out4);
			} ;
			CONTINUE
			
// transition #19 "t19_wen" performing
		:: trans == 19 ->
			c_code {
				get_msOfINT(&now.p2_station_wait, &fT19_in1, now.i1);
				v_id = fT19_in1;
				fT19_in2 = now.p4_env;
				v_I_src = fT19_in2.field_1;
				v_I_type = fT19_in2.field_3;
				v_I_param = fT19_in2.field_4;
				fT19_in3 = now.p14_g_ba;
				v_g_ba = fT19_in3;
				get_msOfLOCALINT(&now.p8_station_next, &fT19_in4, now.i4);
				v_station_next = fT19_in4.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t19_wen");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("g_ba:");
				print_INT(&v_g_ba);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t("");
#endif
				fT19_out1 = v_id;
				put_msOfINT(&now.p2_station_wait, &fT19_out1);
				tmp_INT[0] = v_id;
				fT19_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT19_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_E;
				fT19_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT19_out2.field_4 = tmp_INT[3];
				now.p4_env = fT19_out2;
				fT19_out3 = v_g_ba;
				now.p14_g_ba = fT19_out3;
				tmp_INT[0] = v_id;
				fT19_out4.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT19_out4.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT19_out4);
			} ;
			CONTINUE
			
// transition #20 "t20_wr" performing
		:: trans == 20 ->
			c_code {
				get_msOfINT(&now.p2_station_wait, &fT20_in1, now.i1);
				v_id = fT20_in1;
				fT20_in2 = now.p4_env;
				v_I_src = fT20_in2.field_1;
				v_I_type = fT20_in2.field_3;
				v_I_param = fT20_in2.field_4;
				get_msOfLOCALINT(&now.p7_station_c, &fT20_in3, now.i3);
				v_station_c = fT20_in3.field_2;
				get_msOfLOCALINT(&now.p11_station_maxc, &fT20_in4, now.i4);
				v_station_maxc = fT20_in4.field_2;
				get_msOfLOCALINT(&now.p8_station_next, &fT20_in5, now.i5);
				v_station_next = fT20_in5.field_2;
#ifndef CPN_NOPRINT
				print_t("Transition t20_wr");
				print_t("#");
				print_t(">");
				print_t("I_param:");
				print_INT(&v_I_param);
				print_t("#");
				print_t(">");
				print_t("I_src:");
				print_INT(&v_I_src);
				print_t("#");
				print_t(">");
				print_t("I_type:");
				print_SIGTYPE(&v_I_type);
				print_t("#");
				print_t(">");
				print_t("id:");
				print_INT(&v_id);
				print_t("#");
				print_t(">");
				print_t("station_c:");
				print_INT(&v_station_c);
				print_t("#");
				print_t(">");
				print_t("station_maxc:");
				print_INT(&v_station_maxc);
				print_t("#");
				print_t(">");
				print_t("station_next:");
				print_INT(&v_station_next);
				print_t("#");
				print_t("");
#endif
				fT20_out1 = v_id;
				put_msOfINT(&now.p17_station_idle, &fT20_out1);
				tmp_INT[0] = v_id;
				fT20_out2.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT20_out2.field_2 = tmp_INT[1];
				tmp_SIGTYPE[2] = v_R;
				fT20_out2.field_3 = tmp_SIGTYPE[2];
				tmp_INT[3] = 0;
				fT20_out2.field_4 = tmp_INT[3];
				now.p4_env = fT20_out2;
				tmp_INT[0] = v_id;
				fT20_out3.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxc;
				fT20_out3.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p7_station_c, &fT20_out3);
				tmp_INT[0] = v_id;
				fT20_out4.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_maxc;
				fT20_out4.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p11_station_maxc, &fT20_out4);
				tmp_INT[0] = v_id;
				fT20_out5.field_1 = tmp_INT[0];
				tmp_INT[1] = v_station_next;
				fT20_out5.field_2 = tmp_INT[1];
				put_msOfLOCALINT(&now.p8_station_next, &fT20_out5);
			} ;
			CONTINUE
			
		:: else -> skip
		fi;
		
		if
		:: final ->
			c_code {
				timeNext = 0;
			} ;
			if
			:: c_expr { timeNext } ->
				c_code {
#ifndef CPN_NOPRINT
					print_t("-------------------------------------------  timedec  -------------------------------------------");
					print_t("#");
					print_t(">");
					print_t("#");
					print_t("------------------------------------------- /timedec  -------------------------------------------");
					print_t("#");
					timeNext = 0;
#endif
				} ;
				goto mainLoopInner
			:: else -> 
				c_code {
#ifndef CPN_NOPRINT
					print_t("+++++++++++++++++++++++++++++++++++++++++++|end|+++++++++++++++++++++++++++++++++++++++++++");
					print_t("#");
#endif
					;
				} ;
				assert( c_expr { valid_end_state() } );
			fi
		:: else -> skip
		fi
		; CLR_ALL ;
		final = 0;
		c_code {
			empty_msOfINT(&now.p1_station_reset);
			empty_msOfINT(&now.p2_station_wait);
			empty_msOfINT(&now.p3_station_send);
			null_SIGNAL(&now.p4_env);
			empty_msOfLOCALINT(&now.p5_station_req);
			empty_msOfLOCALINT(&now.p6_station_msgs);
			empty_msOfLOCALINT(&now.p7_station_c);
			empty_msOfLOCALINT(&now.p8_station_next);
			empty_msOfLOCALARRAYINT(&now.p9_station_other);
			empty_msOfLOCALINT(&now.p10_station_ack);
			empty_msOfLOCALINT(&now.p11_station_maxc);
			empty_msOfLOCALINT(&now.p12_station_maxmsgs);
			empty_msOfINT(&now.p13_NonDeterm0);
			null_INT(&now.p14_g_ba);
			null_INT(&now.p15_g_dst);
			null_INT(&now.p16_g_cnt);
			empty_msOfINT(&now.p17_station_idle);
		}
		;
end_state:
		if
		:: 0 -> skip // end state
		fi
		;
stopped:
		skip
	} // end atomic, no more transitions
}

#define atmr_msgs3_1_1_as_txt_cpn
#define fin cpn@end_state
#include "ltl_defines.pml.h"
