// <timed-datatype-methods> (methods cmp_##TYPENAME & null_##TYPENAME should be defined)
#define GEN_TIMED_METHODS(TIMEDNAME, TYPENAME) \
\
typedef struct cs_##TIMEDNAME { \
	inttype time; \
	cs_##TYPENAME inner; \
} cs_##TIMEDNAME; \
 \
void null_##TIMEDNAME(cs_##TIMEDNAME *timed) { \
	timed->time = 0; \
	null_##TYPENAME(&timed->inner); \
} \
 \
void print_##TIMEDNAME(cs_##TIMEDNAME *timed) { \
	print_t("TIMED /"); \
	print_##TYPENAME(&timed->inner); \
	print_t("- @ -"); \
	print_INT(&timed->time); /* is it OK? */ \
	print_t("/"); \
} \
 \
int cmp_##TIMEDNAME(cs_##TIMEDNAME *a, cs_##TIMEDNAME *b) { \
	int r = a->time - b->time; \
	if (r <= 0) return cmp_##TYPENAME(&a->inner, &b->inner); \
	else return r; \
} \
 \
void time_##TIMEDNAME(cs_##TIMEDNAME *res, cs_##TYPENAME *val, inttype *time) { \
	res->time = *time; \
	res->inner = *val; \
} \
 \
void addtime_##TIMEDNAME(cs_##TIMEDNAME *res, cs_##TIMEDNAME *val, inttype *time) { \
	res->time = val->time + *time; \
	res->inner = val->inner; \
}
// </ timed-datatype-methods>

void decTime(inttype *time, inttype nextTime) {
	if (*time > 0) {
		*time -= nextTime;
		//assert(time >= 0);
	}
}
void nextTime(inttype time, inttype *pt, inttype *nextTime) {
	while (*pt >= time && *pt > 0) ++pt;
	if (time > *pt) {
		if (*nextTime > time || *nextTime == 0) *nextTime = time;
	}
}
