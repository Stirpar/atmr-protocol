// <list-datatype-methods> (LIST_MAX_LENGTH and methods cmp_##TYPENAME & null_##TYPENAME should be defined)
#define GEN_LIST_METHODS(LISTNAME, TYPENAME) \
\
typedef struct cs_##LISTNAME { \
	inttype length; \
	cs_##TYPENAME content[LIST_MAX_LENGTH]; \
} cs_##LISTNAME; \
 \
void null_##LISTNAME(cs_##LISTNAME *list) { \
	int i; \
	list->length = 0; \
	for (i = 0; i < LIST_MAX_LENGTH; ++i) { \
		null_##TYPENAME(&list->content[i]); \
	} \
} \
 \
void clr_##LISTNAME(cs_##LISTNAME *list) { \
	int i; \
	for (i = list->length; i < LIST_MAX_LENGTH; ++i) { \
		null_##TYPENAME(&list->content[i]); \
	} \
} \
 \
void print_##LISTNAME(cs_##LISTNAME *list) { \
	int i; \
	print_t("LIST ["); \
	for (i = 0; i < list->length; ++i) { \
		print_##TYPENAME(&list->content[i]); \
	} \
	print_t("]"); \
} \
 \
int cmp_##LISTNAME(cs_##LISTNAME *a, cs_##LISTNAME *b) { \
	int i; \
	for (i = 0; i < LIST_MAX_LENGTH; ++i) { \
		if (i == a->length) { \
			if (i == b->length) return 0; \
			else return -1; \
		} else { \
			if (i == b->length) return 1; \
			else { \
				int r = cmp_##TYPENAME(&a->content[i], &b->content[i]); \
				if (r != 0) return r; \
			} \
		} \
	} \
} \
 \
void nth_##LISTNAME(cs_##TYPENAME *res, cs_##LISTNAME *list, int n) { \
	*res = list->content[n]/*indices starting from 0*/; \
} \
 \
int fails_nth_##LISTNAME(int *expr_error, int *evaluation_fail,cs_##LISTNAME *list, int n) { \
	if (n >= list->length) return *expr_error = 1; \
	return 0; \
} \
 \
void nthtail_##LISTNAME(cs_##LISTNAME *res, cs_##LISTNAME *list, int n) { \
	int i; \
	res->length = list->length - n - 1 /*indices starting from 0*/; \
	for (i = n + 1; i < list->length; ++i) { \
		res->content[i - n - 1] = list->content[i]; \
	} \
	clr_##LISTNAME(res); \
} \
 \
int fails_nthtail_##LISTNAME(int *expr_error, int *evaluation_fail,cs_##LISTNAME *list, int n) { \
	if (n >= list->length) return *expr_error = 1; \
	return 0; \
} \
 \
void hd_##LISTNAME(cs_##TYPENAME *res, cs_##LISTNAME *list) { \
	nth_##LISTNAME(res, list, 0); \
} \
 \
int fails_hd_##LISTNAME(int *expr_error, int *evaluation_fail, cs_##LISTNAME *list) { \
	return fails_nth_##LISTNAME(expr_error, evaluation_fail, list, 0); \
} \
 \
void tl_##LISTNAME(cs_##LISTNAME *res, cs_##LISTNAME *list) { \
	nthtail_##LISTNAME(res, list, 0); \
} \
 \
int fails_tl_##LISTNAME(int *expr_error, int *evaluation_fail, cs_##LISTNAME *list) { \
	return fails_nthtail_##LISTNAME(expr_error, evaluation_fail, list, 0); \
} \
 \
void nthreplace_##LISTNAME(cs_##LISTNAME *res, cs_##LISTNAME *list, int n, cs_##TYPENAME *elem) { \
	int i; \
	res->length = list->length; \
	for (i = 0; i < n; ++i) { \
		res->content[i] = list->content[i]; \
	} \
	res->content[n] = *elem;/*indices starting from 0*/ \
	for (i = n + 1; i < list->length; ++i) { \
		res->content[i] = list->content[i]; \
	} \
	clr_##LISTNAME(res); \
} \
 \
int fails_nthreplace_##LISTNAME(int *expr_error, int *evaluation_fail, cs_##LISTNAME *list, int n, cs_##TYPENAME *elem) { \
	if (n >= list->length) return *expr_error = 1; \
	return 0; \
} \
 \
void cons_##LISTNAME(cs_##LISTNAME *res, cs_##TYPENAME *elem, cs_##LISTNAME *list) { \
	int i; \
	res->length = list->length + 1; \
	res->content[0] = *elem; \
	for (i = 0; i < list->length; ++i) { \
		res->content[i + 1] = list->content[i]; \
	} \
	clr_##LISTNAME(res); \
} \
 \
int fails_cons_##LISTNAME(int *expr_error, int *evaluation_fail, cs_##TYPENAME *elem, cs_##LISTNAME *list) { \
	if (list->length >= LIST_MAX_LENGTH) return *evaluation_fail = 1; \
	return 0; \
} \
 \
void concat_##LISTNAME(cs_##LISTNAME *res, cs_##LISTNAME *list1, cs_##LISTNAME *list2) { \
	int i; \
	res->length = list1->length + list2->length; \
	for (i = 0; i < list1->length; ++i) { \
		res->content[i] = list1->content[i]; \
	} \
	for (i = 0; i < list2->length; ++i) { \
		res->content[i + list1->length] = list2->content[i]; \
	} \
	clr_##LISTNAME(res); \
} \
 \
int fails_concat_##LISTNAME(int *expr_error, int *evaluation_fail, cs_##LISTNAME *list1, cs_##LISTNAME *list2) { \
	if (list1->length + list2->length >= LIST_MAX_LENGTH) return *evaluation_fail = 1; \
	return 0; \
} \
 \
void rev_##LISTNAME(cs_##LISTNAME *res, cs_##LISTNAME *list) { \
	int i; \
	res->length = list->length; \
	for (i = 0; i < list->length; ++i) { \
		res->content[i] = list->content[list->length - i - 1]; \
	} \
	clr_##LISTNAME(res); \
} \
 \
int fails_rev_##LISTNAME(int *expr_error, int *evaluation_fail, cs_##LISTNAME *list) { \
	return 0; \
} \
 \
void rmall_##LISTNAME(cs_##LISTNAME *res, cs_##TYPENAME *elemval, cs_##LISTNAME *list) { \
	int i; \
	res->length = 0; \
	for (i = 0; i < list->length; ++i) { \
		if (! cmp_##TYPENAME(elemval, &list->content[i])) continue; \
		res->content[res->length++] = list->content[i]; \
	} \
	clr_##LISTNAME(res); \
} \
 \
int fails_rmall_##LISTNAME(int *expr_error, int *evaluation_fail, cs_##TYPENAME *elemval, cs_##LISTNAME *list) { \
	return 0; \
}

// TODO support in translator:
/*
int contains_##LISTNAME(cs_##TYPENAME *elemval, cs_##LISTNAME *list) { \
	int i; \
	for (i = 0; i < list->length; ++i) { \
		if (! cmp_##TYPENAME(elemval, &list->content[i])) return 1; \
	} \
	return 0; \
}
*/
// </ list-datatype-methods>
